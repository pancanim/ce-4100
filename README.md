# Alarm Clock Project #

This public repository is used for the distribution of the firmware of the Alarm Clock project for the Fall 2016 CE-4100 Embedded Systems Fabrication class at Milwaukee School of Engineering (MSOE).

### Info ###

This Repository has two folders:

* Code: Contains the source code and Eclipse project files
* Other: Conatins some documentation about the implementation of the firmware and the use cases of the alarm clock

### Set-up ###

** Note: ** This set-up is assuming that the bootloader has already been flashed on the board, so that the USB port could be used.

* Install Eclipse IDE for C/C++ Developers (Latest release at the time of writing is Eclipse Neon)
https://www.eclipse.org/downloads/packages/eclipse-ide-cc-developers/neon3

* Install Java Developent Kit (JDK) or Java Runtime Environment (JRE) (Latest release at the time of writing is 8u131)
http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html

* Install WinAVR and Arduino folder
https://faculty-web.msoe.edu/prust/arduino/index.html

* Follow this tutorial until the "Using Eclipse" section (To configure the AVR plug-in and hardware drivers, see below)
https://faculty-web.msoe.edu/prust/arduino/arduinoResources/arduinoUNO.eclipse.pdf

* Follow the "Install and configure the AVR-Eclipse plugin" section of this tutorial
https://protostack.com.au/2010/12/avr-eclipse-environment-on-windows/

* Plug-in the Alarm Clock to the PC's USB port, go to the Device Manager and install the hardware drivers by using the "Browse my computer" option selecting the Arduino folder

* When creating a new configuration with the AVRDude (Dr. Prust's tutorial), select 'Arduino' as the 'Programmer Hardware' and set the default baudrate to 115200.

* Download Code folder from this repository and unzip it

* In Eclipse, import the project with File>Open Project From File System

* Right click on the project, Properties>AVRDude and, under Programmer Configuration select the configuration created earlier

* Build the project

* Use the AVR green arrow to load the executable on the Alarm Clock

### Libraries ###

For this project, the following two open-source libraries were used:

* ulibSD: Needed to interface the SD card via SPI
https://github.com/1nv1/ulibSD

* Petit FatFs: Light version of the FatFs library, used to manage the FAT file system on the SD card
http://elm-chan.org/fsw/ff/00index_p.html

### Usage ###

See States.docx in the 'Other' folder of this repository

### HOW TO SET A CUSTOM RING ALARM TONE ###

In order to play a custom audio file when the alarm goes off, a .WAV file named 'alarm.wav' needs to be placed in the root directory of the SD card. However, due to speed and memory constraints of the ATmega644p microprocessor, the following limitations apply:

* WAV file must be **8-bit**, **mono channel** with sampling rate (byte rate) **less than 11.025 kHz**

It is sugested to use the following website to convert any .mp3 audio file to an 8-bit .wav with the aforementioned parameters:
http://audio.online-convert.com/convert-to-wav

* SD card should be less than 64 MB and formatted as FAT12 or FAT16. Theoretically, even SDHC cards formatted as FAT32 should work, as they are supported by the libraries, but I have tried 3 different SDHC's and none of them worked..

**Note:** To verify if the SD card and the audio file work correctly, insert the SD card and press '5'.. This will trigger the alarm and one of the following three things will happen (Make sure the volume is not 0 by turning it up using key '9'):

* If the alarm sounds like two "beeps" (default alarm), then either the SD card or the file system is not compatible.

* If the speaker buzzes or plays some weired noise, then the .wav file is not formatted properly.

* If the audio file plays correctly, then everything works fine.

**Note 2:** Because of resources limitations, the display cannot be refreshed while the audio is playing and vice versa. Therefore, even though there is no limit to the length of the audio file, it is suggested to choose a ring tone that lasts a few seconds. This way, the ring tone will play, then the display will blink showing the time and the process will repeat in a loop until the user either snoozes the alarm pressing the snooze bar, stops the alarm by pressing any other key or the alarm times out automatically after 5 minutes.

### Contacts ###

If you have any question or need assistance setting up the programming environment, feel free to contact me at pancanim@alumni.msoe.edu.
Feedback is always appreciated too!