/**
 * CE4100
 * Michele Pancani
 *
 * HEADER FILE
 *
 * Description:
 * Implemented: 05/25/17
 * Edited: 05/28/17
 *
 */

#ifndef AUDIOPLAYER_H_
#define AUDIOPLAYER_H_

#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <avr/io.h>
#include <string.h>

#include "FATLib/pff.h"
#include "I2C.h"
#include "Display.h"

#define AUDIO_BUFFER_SIZE 2048	// This number is ideal because it buffers enough audio samples without using too much RAM, which is very limited
#define WAV_DATA_SECTION_SUBCHUNK_ID ((uint32_t) 0x61746164)	// ID ("data") is in Little Endian form

// Header of the WAV file
typedef struct {

	uint32_t chunkID;
	uint32_t chunkSize;
	uint32_t format;
	uint32_t subChunk1ID;
	uint32_t subChunk1Size;
	uint16_t audioFormat;
	uint16_t numOfChannels;
	uint32_t sampleRate;
	uint32_t byteRate;
	uint16_t blockAlign;
	uint16_t bitsPerSample;

} __attribute__((packed)) WAV_HEADER;

// Subchunks following the WAV header before the raw data
typedef struct {
	uint32_t subChunk2ID;
	uint32_t subChunk2Size;
}__attribute__((packed)) WAV_HEADER_SUBCHUNK2;

/********************************************************************************
 * Function name: speakerOn
 * Operation: Turn on the speaker
 * Inputs: None
 * Returns: None
 * Alters: None
 * Implemented:
 * Edited:
 *********************************************************************************/
void speakerOn(void);

/********************************************************************************
 * Function name: speakerOff
 * Operation: Turn off the speaker
 * Inputs: None
 * Returns: None
 * Alters: None
 * Implemented:
 * Edited:
 *********************************************************************************/
void speakerOff(void);

/********************************************************************************
 * Function name: DACInit
 * Operation: Set the direction of the speaker's pin and turn it off. Default DAC
 * 				setting are good as they are.
 * Inputs: None
 * Returns: true if successful initialization, false otherwise
 * Alters: None
 * Implemented:
 * Edited:
 *********************************************************************************/
bool DACInit(void);

/********************************************************************************
 * Function name: playNextAudioSample_ISR
 * Operation: Triggered by a fixed-interval interrupt, read an audio sample from the
 * 				audio circular buffer, adjust the volume and send it to the DAC
 * Inputs: None
 * Returns: None
 * Alters: None
 * Implemented:
 * Edited:
 *********************************************************************************/
void playNextAudioSample_ISR(void);

/********************************************************************************
 * Function name: playAudioFile
 * Operation: Load the specified .wav file from the SD card and fill the audio
 * 				circular buffer as the song is being played through the
 * 				playNextAudioSample_ISR
 * Inputs: fileName	Name of the .wav file to be loaded from the SD card and played
 * Returns: true if the .wav file was successfully loaded and played, false if something failed
 * Alters: None
 * Implemented:
 * Edited:
 *********************************************************************************/
bool playAudioFile(char *fileName);

/********************************************************************************
 * Function name: playDefaultAlarm
 * Operation: Fill the audio circular buffer with a 5.5 kHz square wave and play
 * 				the tone for 100ms twice through playNextAudioSample_ISR
 * Inputs: None
 * Returns: None
 * Alters: None
 * Implemented:
 * Edited:
 *********************************************************************************/
void playDefaultAlarm(void);

#endif /* AUDIOPLAYER_H_ */
