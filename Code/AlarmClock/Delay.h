/**
 * CE4100
 * Michele Pancani
 *
 * HEADER FILE
 *
 * Description:
 * Implemented: 09/29/16
 * Edited:
 *
 */

#ifndef DELAY_H_
#define DELAY_H_

#include <avr/io.h>
#include <avr/interrupt.h>

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>

#define PIE 7
#define CPU_FREQ_MHZ 20

/**
 * NOTE ABOUT TIMERS USAGE:
 * - Timer 0 (8-bit): Used for blocking delays
 * - Timer 1 (16-bit): Used to trigger interrupts at fixed intervals in order
 * 				to refresh the display or play audio samples.
 * - Timer 2 (8-bit): Used for the non-blocking delay
 */

/********************************************************************************
 * Function name: delay_ms
 * Operation: Blocking delay function
 * Inputs: Delay in ms (Max 2^16 - 1)
 * Returns: None
 * Alters: None
 * Implemented: 09/29/16
 * Edited: 	10/23/16
 * 			11/11/16 MP
 *********************************************************************************/
void delay_ms(uint16_t ms);

/********************************************************************************
 * Function name: delay_us
 * Operation: Blocking delay function
 * Inputs: Delay in us (Max 2^16 - 1)
 * Returns: None
 * Alters: None
 * Implemented: 10/29/16
 * Edited:	11/11/16
 *********************************************************************************/
void delay_us(uint16_t us);

/********************************************************************************
 * Function name: start1msSysClock
 * Operation: Start the non-blocking 1 ms counter
 * Inputs: none
 * Returns: None
 * Alters: None
 * Implemented:
 * Edited:
 *********************************************************************************/
void start1msSysClock(void);

/********************************************************************************
 * Function name: stop1msSysClock
 * Operation: Stop the non-blocking 1 ms counter
 * Inputs: none
 * Returns: None
 * Alters: None
 * Implemented:
 * Edited:
 *********************************************************************************/
void stop1msSysClock(void);

#endif /* DELAY_H_ */
