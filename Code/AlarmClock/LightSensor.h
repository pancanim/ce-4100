/**
 * CE4100
 *
 * HEADER FILE
 *
 * Description:
 * Implemented: 09/29/16
 * Edited:
 *
 */

#ifndef LIGHTSENSOR_H_
#define LIGHTSENSOR_H_

#include <avr/io.h>
#include <avr/interrupt.h>

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include "delay.h"

/********************************************************************************
 * Function name: LightSensorInit
 * Operation:  Initialize light sensor
 * Inputs: None
 * Returns: None
 * Alters: None
 * Implemented:
 * Edited:  11/3/2016
 *********************************************************************************/
// Write function header here

void LightSensorInit(void);

/********************************************************************************
 * Function name: readLightIntensityPercentage
 * Operation: read the intensity into the ADC and convert it to a percentage
 * Inputs: None
 * Returns: None
 * Alters: None
 * Implemented:
 * Edited: 11/3/2016
 *********************************************************************************/
// Write function header here

uint8_t readLightIntensityPercentage(void);

#endif /* LIGHTSENSOR_H_ */
