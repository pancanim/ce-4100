/**
 * CE4100
 *
 * IMPLEMENTATION FILE
 *
 * Description:
 * Implemented:
 * Edited:
 *
 */

#include "LightSensor.h"

// Write function implementation here
//Light sensor comes into pin 30 (from schematic)
//is it possible to include arduino.h, then I can use analogread and this becomes much easier
//Pin 30 is PA7 (ADC7/PCINT7)
//values from ADC will range from 0 to 1023, to we want to convert to a percentage from this, so divide by 10.23

uint16_t readanalog = 0;
uint8_t lightintensitypercentage;

//left adjusted result yields 8 bit adc conversion in ADCH
//"if a lower resolution than 10 bits is needed, the input clock freq can be higher than 200kHz"
//25 clock cycles per conversion first time, 13 after that
//ADC switched on using ADEN
//write a 1 to ADC start conversion bit, ADSC

void LightSensorInit (void) {

//	DIDR0 = 0x80; 		//"when an analog signal is applied to the adc7 pin and digital input is not needed, this bit should be written logic one to reduce power consumption"
//	PRR &= ~(1<<PRADC); //writing a 1 to this bit shuts down the ADC
//	ADMUX = 0b01100111; //this setting will : set VREF to AVCC, left adjusted result no differential input and uses ADC7 as input
//	ADCSRA = 0xC7;		//ADC enabled, no auto trigger, interrupt enabled, 128 prescalar
//	//0xCF is 11001111
//	//ADCSRA: ADEN ADSC ADATE ADIF ADIE ADPS2 ADPS1 ADPS0
}

uint8_t readLightIntensityPercentage(void){

//	ADCSRA |= 1<<ADSC;  	//conversion start
//	delay_us(2);
//	readanalog |= (ADCH << 2)|(ADCL>>6); //readanalog will have the 8 higher bits shifted left 2 to make room for the 2 lower bits, so in this bit 0 through bit 9 are correct, now to scale
//	lightintensitypercentage = readanalog/10.23;
//	return lightintensitypercentage;
	return 0;

}
