/**
 * CE4100
 * Michele Pancani
 *
 * HEADER FILE
 *
 * Description:
 * Implemented: 12/18/16
 * Edited: 05/28/17
 *
 */

#ifndef RTC_H_
#define RTC_H_

#include <avr/io.h>
#include <avr/interrupt.h>

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#include "Delay.h"
#include "Display.h"
#include "I2C.h"

struct timeStruct {
	uint8_t hours;
	uint8_t minutes;
	uint8_t am_pm;
	uint8_t reg_offset;
};

struct dateStruct {
	uint8_t day;
	uint8_t weekDay;
	uint8_t month;
	uint16_t year;
};

#define RTC_SECS_REG 0x00
#define RTC_MINS_REG 0x01
#define RTC_HOURS_REG 0x02
#define RTC_DAY_REG 0x03
#define RTC_DATE_REG 0x04
#define RTC_MONTH_REG 0x05
#define RTC_YEAR_REG 0x06
#define RTC_ALARM1_SECS_REG 0x07
#define RTC_ALARM1_MINS_REG 0x08
#define RTC_ALARM1_HOURS_REG 0x09
#define RTC_ALARM1_DAYDATE_REG 0x0A
#define RTC_ALARM2_MINS_REG 0x0B
#define RTC_ALARM2_HOURS_REG 0x0C
#define RTC_ALARM2_DAYDATE_REG 0x0D
#define RTC_CONTROL_REG 0x0E
#define RTC_STATUS_REG 0x0F
#define RTC_TEMPERATURE_MSB_REG 0x11
#define RTC_TEMPERATURE_LSB_REG 0x12
#define RTC_ALARM1_TIME_OFFSET 0x07
#define RTC_ALARM2_TIME_OFFSET 0x0A

#define RTC_TENS_BIT 4
#define RTC_AM_PM_BIT 5
#define RTC_12_24_BIT 6

#define A1IE 0
#define A2IE 1
#define INTCN 2

#define A1F 0
#define A2F 1

#define A1M1 7
#define A1M2 7
#define A1M3 7
#define A1M4 7

#define A2M1 7
#define A2M2 7
#define A2M3 7
#define A2M4 7

// Bit 4 in the control register, NA, is described as
	// "NA: Not applicable. These bits have no affect on
	// the device and can be set to either 0 or 1."
	// The default is '1', therefore is it will be used
	// to determine if the RTC is being initialized for the
	// first time (1) or not (0).
#define MY_BIT 4

/********************************************************************************
 * Function name: RTCInit
 * Operation: Initialize the RTC and set default values if never initialized before
 * Inputs: None
 * Returns: true if the initialization was successful, false otherwise
 * Alters: None
 * Implemented: 12/19/16
 * Edited: 05/28/17
 *********************************************************************************/
bool RTCInit(void);

/********************************************************************************
 * Function name: updateTimeFromRTC
 * Operation: Read the current time from the RTC and stores it in the given time struct
 * Inputs: time	pointer to the time struct in which the time will be stored
 * Returns: None
 * Alters: None
 * Implemented: 12/18/16
 * Edited:
 *********************************************************************************/
bool updateTimeFromRTC(struct timeStruct *time);

/********************************************************************************
 * Function name: updateDateFromRTC
 * Operation: Read the current date from the RTC and stores it in the given date struct
 * Inputs: date	pointer to the date struct in which the date will be stored
 * Returns: None
 * Alters: None
 * Implemented: 12/18/16
 * Edited:
 *********************************************************************************/
bool updateDateFromRTC(struct dateStruct *date);

/********************************************************************************
 * Function name: isAlarm1On
 * Operation: Return the status of Alarm 1
 * Inputs: None
 * Returns: true if Alarm 1 is on
 * Alters: None
 * Implemented: 05/28/17
 * Edited:
 *********************************************************************************/
bool isAlarm1On(void);

/********************************************************************************
 * Function name: isAlarm2On
 * Operation: Return the status of Alarm 2
 * Inputs: None
 * Returns: true if Alarm 2 is on
 * Alters: None
 * Implemented: 05/28/17
 * Edited:
 *********************************************************************************/
bool isAlarm2On(void);

/********************************************************************************
 * Function name: toggleAlarm1
 * Operation: Toggle the status of Alarm 1
 * Inputs: None
 * Returns: None
 * Alters: None
 * Implemented: 05/28/17
 * Edited:
 *********************************************************************************/
void toggleAlarm1(void);

/********************************************************************************
 * Function name: setAlarm2
 * Operation: Set alarm 2, used for snoozing
 * Inputs:	hours		Hours
 * 			minutes		Minutes
 * 			snoozeVal	Additional minutes to add to the given hours and minutes
 * Returns: None
 * Alters: None
 * Implemented: 05/28/17
 * Edited:
 *********************************************************************************/
void setAlarm2(uint8_t hours, uint8_t minutes, uint8_t snoozeVal);

/********************************************************************************
 * Function name: alarm2OnOff
 * Operation: Turn on/off alarm 2, used for snoozing
 * Inputs:	alarmOn	turn on the alarm if true, turn it off if false
 * Returns: None
 * Alters: None
 * Implemented: 05/28/17
 * Edited:
 *********************************************************************************/
void alarm2OnOff(bool alarmOn);

/********************************************************************************
 * Function name: clearAlarm1InterruptFlag
 * Operation: Clear the interrupt flag of alarm 1
 * Inputs:	None
 * Returns: None
 * Alters: None
 * Implemented: 05/28/17
 * Edited:
 *********************************************************************************/
void clearAlarm1InterruptFlag(void);

/********************************************************************************
 * Function name: clearAlarm2InterruptFlag
 * Operation: Clear the interrupt flag of alarm 2, used for snoozing
 * Inputs:	None
 * Returns: None
 * Alters: None
 * Implemented: 05/28/17
 * Edited:
 *********************************************************************************/
void clearAlarm2InterruptFlag(void);

#endif /* RTC_H_ */
