/*
 * spi_io.c
 *
 *  Created on: 13 apr 2017
 *      Author: Michele Pancani
 */

#include "spi_io.h"
#include <avr/io.h>
#include <stdio.h>
#include "../Delay.h"		// 2017.04.30 MP

#define SPI_CS		PB4	// 2017.03.19 MP
#define SPI_MOSI	PB5	// 2017.03.19 MP
#define SPI_MISO	PB6	// 2017.03.19 MP
#define SPI_CLK		PB7	// 2017.03.19 MP

extern uint16_t gSysClockCountMs;	// 2017.04.30 MP

/**
    \brief Initialize SPI hardware
 */
void SPI_Init (void) {

	//master,little endian, SCK phase low, SCK idle low//
	DDRB  |= ((1<<SPI_MOSI)|(1<<SPI_CLK)|(1<<SPI_CS));	//2017.03.20 MP - Set MOSI, CLK and CS as output
	DDRB  &= ~(1<<SPI_MISO);							//2017.03.20 MP - Set MISO as input
	SPCR = 0x00;
	SPSR = 0x00;
	SPCR |= (1<<SPE|1<<MSTR);

}

/**
    \brief Read/Write a single byte.
    \param d Byte to send.
    \return Byte that arrived.
 */
BYTE SPI_RW (BYTE d) {

	SPDR = d;
	while ( ! (SPSR & ( 1 << SPIF ) ) );
	return SPDR;

}

/**
    \brief Flush of SPI buffer.
 */
void SPI_Release (void) {

}

/**
    \brief Selecting function in SPI terms, associated with SPI module.
 */
void SPI_CS_Low (void) {
	PORTB &= ~(1<<SPI_CS);
}

/**
    \brief Deselecting function in SPI terms, associated with SPI module.
 */
void SPI_CS_High (void) {
	PORTB |= (1<<SPI_CS);
}

/**
    \brief Setting frequency of SPI's clock to maximum possible.
 */
void SPI_Freq_High (void) {
	// fClk/2 = 10 MHz
	SPCR &= ~(1<<SPR1|1<<SPR0);
	SPSR |= ( 1 << SPI2X );
}

/**
    \brief Setting frequency of SPI's clock equal or lower than 400kHz.
 */
void SPI_Freq_Low (void) {
	// fClk/64 = 312.5 kHz
	SPCR |= (1<<SPR1|1<<SPR0);
	SPSR |= ( 1 << SPI2X );
}

/**
    \brief Start a non-blocking timer.
    \param ms Milliseconds.
 */
void SPI_Timer_On (WORD ms) {
	gSysClockCountMs = ms;
	start1msSysClock();
}

/**
    \brief Check the status of non-blocking timer.
    \return Status, TRUE if timeout is not reach yet.
 */
BOOL SPI_Timer_Status (void) {
	return gSysClockCountMs > 0 ? TRUE : FALSE;
}

/**
    \brief Stop of non-blocking timer. Mandatory.
 */
void SPI_Timer_Off (void) {
	stop1msSysClock();
}
