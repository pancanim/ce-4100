/**
 * CE4100
 * Michele Pancani
 *
 * IMPLEMENTATION FILE
 *
 * Description:
 * Implemented: 12/18/16
 * Edited:
 *
 */

#include "I2C.h"

static void i2cInterruptEnable(bool enable);
static void i2cInterruptEnable(bool enable) {

	if(enable) {
		TWCR |= (1<<TWIE);
	} else {
		TWCR &= ~(1<<TWIE);
	} // End if

}

void i2c_init(void) {

	// Set Bit rate for 400kHz I2C interface (357.143kHz)
		// SCL_freq = CPU_clk_freq / (16+2(TWBR)*4^TWPS)
	// Set prescaler TWPS to 1
	TWSR &= ~(1<<TWPS1|1<<TWPS0);
	// Set Bit rate division factor to 5
	TWBR = SCL_DIVISION_FACTOR;

	// Enable I2C Peripheral
	TWCR |= (1<<TWEN);

}

uint8_t i2c_Write(uint8_t address, uint8_t reg, uint8_t data) {

	// Save interrupt settings and temporary disable interrupts
	uint8_t interruptStatus = (TWCR & TWIE);
	i2cInterruptEnable(false);

	uint8_t i2cStatus;

	// Generate Start Condition
	TWCR = (1<<TWINT|1<<TWSTA|1<<TWEN);
	// Wait
	while(!(TWCR & (1<<TWINT)));

	i2cStatus = (TWSR & 0xF8);
	if(i2cStatus != 0x08) {		// Error
		return i2cStatus;
	} // End if

	// Start condition has been transmitted
	// Load data register with device's address + W bit
	TWDR = address|I2C_WRITE;
	// Transmit
	TWCR = (1<<TWINT|1<<TWEN);
	// Wait
	while(!(TWCR & (1<<TWINT)));

	i2cStatus = (TWSR & 0xF8);
	if(i2cStatus != 0x18) {		// Error
		return i2cStatus;
	} // End if

	// Address+W has been transmitted and acknowledged
	// Load data byte (target register's address)
	TWDR = reg;
	// Transmit
	TWCR = (1<<TWINT|1<<TWEN);
	// Wait
	while(!(TWCR & (1<<TWINT)));

	i2cStatus = (TWSR & 0xF8);
	if(i2cStatus != 0x28) {		// Error
		return i2cStatus;
	} // End if

	// Data byte has been transmitted and acknowledged
	// Load data byte (target register's data)
	TWDR = data;
	// Transmit
	TWCR = (1<<TWINT|1<<TWEN);
	// Wait
	while(!(TWCR & (1<<TWINT)));

	i2cStatus = (TWSR & 0xF8);
	if(i2cStatus != 0x28) {		// Error
		return i2cStatus;
	} // End if

	// Generate Stop condition
	TWCR = (1<<TWSTO|1<<TWEN);

	// Restore interrupt settings
	TWCR |= interruptStatus;

	return 0;					// No errors

}

uint8_t i2c_Read(uint8_t address, uint8_t reg) {

	uint8_t receivedData = -1;

	// Save interrupt settings and temporary disable interrupts
	uint8_t interruptStatus = (TWCR & TWIE);
	i2cInterruptEnable(false);

	uint8_t i2cStatus;

	// Generate Start Condition
	TWCR = (1<<TWINT|1<<TWSTA|1<<TWEN);
	// Wait
	while(!(TWCR & (1<<TWINT)));

	i2cStatus = (TWSR & 0xF8);
	if(i2cStatus != 0x08) {		// Error
		//return i2cStatus;
		return -1;
	} // End if

	// Start condition has been transmitted
	// Load data register with device's address + W bit
	TWDR = address|I2C_WRITE;
	// Transmit
	TWCR = (1<<TWINT|1<<TWEN);
	// Wait
	while(!(TWCR & (1<<TWINT)));

	i2cStatus = (TWSR & 0xF8);
	if(i2cStatus != 0x18) {		// Error
		//return i2cStatus;
		return -1;
	} // End if

	// Address+W has been transmitted and acknowledged
	// Load data byte (target register's address)
	TWDR = reg;
	// Transmit
	TWCR = (1<<TWINT|1<<TWEN);
	// Wait
	while(!(TWCR & (1<<TWINT)));

	i2cStatus = (TWSR & 0xF8);
	if(i2cStatus != 0x28) {		// Error
		//return i2cStatus;
		return -1;
	} // End if

	// Data byte has been transmitted and acknowledged
	// Generate repeated start condition
	TWCR = (1<<TWINT|1<<TWSTA|1<<TWEN);
	// Wait
	while(!(TWCR & (1<<TWINT)));

	i2cStatus = (TWSR & 0xF8);
	if(i2cStatus != 0x10) {		// Error
		//return i2cStatus;
		return -1;
	} // End if

	// Repeated start condition has been transmitted
	// Load data register with device's address + R bit
	TWDR = address|I2C_READ;
	// Transmit
	TWCR = (1<<TWINT|1<<TWEN);
	// Wait
	while(!(TWCR & (1<<TWINT)));

	i2cStatus = (TWSR & 0xF8);
	if(i2cStatus != 0x40) {		// Error
		//return i2cStatus;
		return -1;
	} // End if

	// Address+R has been transmitted and acknowledged
	// Receive data
	TWCR = (1<<TWINT|1<<TWEN);
	// Wait
	while(!(TWCR & (1<<TWINT)));

	i2cStatus = (TWSR & 0xF8);
	if(i2cStatus != 0x58) {		// Error
		//return i2cStatus;
		return -1;
	} // End if

	// Data byte has been received and NOT ack has been returned
	// Read Data
	receivedData = TWDR;

	// Generate Stop condition
	TWCR = (1<<TWSTO|1<<TWEN);

	// Restore interrupt settings
	TWCR |= interruptStatus;

	return receivedData;					// No errors

}
