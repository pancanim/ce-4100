/**
 * CE4100
 * Michele Pancani
 *
 * HEADER FILE
 *
 * Description:
 * Implemented: 10/22/16
 * Edited: 05/28/17
 *
 */

#ifndef STATES_H_
#define STATES_H_

#include <avr/io.h>
#include <avr/interrupt.h>

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>

#include "Delay.h"
#include "Display.h"
#include "Keypad.h"
#include "RTC.h"
#include "I2C.h"
#include "AudioPlayer.h"

#define KEY_HOLD_DELAY_MS 2000					// Millisecond delay '*', '#' and the snooze bar needs to be held down before changing state
#define FLASH_SETTING_CHANGE_DELAY_MS 2000		// Millisecond delay used to temporary display the value of volume, temperature and snooze on the screen
#define VOLUME_PERCENTAGE_INCREMENT 10			// Percent increment used to turn the volume up/down
#define ALARM_TIMEOUT_MINS 5					// Max 59 mins. Number of minutes after which an ongoing alarm times out
#define SNOOZE_MINUTES_INCREMENT 5				// Minutes increment used to set the snooze time
#define SNOOZE_MINUTES_MAX 90	// Snooze should be no longer than 99 mins, otheriwse displayDispSettingsValue() needs to be changed
#define ALARM_AUDIO_FILE_NAME (char*) "alarm.wav"	// Name of the 8-bit .wav file that will be played from the SD card when the alarm goes off

/**
 * States of the state machine
 */
typedef uint8_t STATE;
enum STATE {RESET, DISPLAY_TIME, SET_TIME, SET_ALARM, ALARM, SETTINGS, ERROR};

/********************************************************************************
 * Function name: reset
 * Operation: Reset the alarm clock, except for the values saved in the RTC, such as
 * 				time, date and alarms
 * Inputs: None
 * Returns: None
 * Alters: None
 * Implemented: 10/22/16
 * Edited: 05/28/17
 *********************************************************************************/
void reset(void);

/********************************************************************************
 * Function name: displayTime
 * Operation: Display the current time and date on the screen and waits for a key
 * 				to be pressed
 * Inputs: None
 * Returns: None
 * Alters: None
 * Implemented: 10/22/16
 * Edited:
 *********************************************************************************/
void displayTime(void);

/********************************************************************************
 * Function name: Set Time
 * Operation: Allow the user to change the current time and date
 * Inputs: None
 * Returns: None
 * Alters: None
 * Implemented: 10/22/16
 * Edited:
 *********************************************************************************/
void setTime(void);

/********************************************************************************
 * Function name: Allow the user to set an alarm that will go off daily, if
 * 					enabled in the settings
 * Operation:
 * Inputs: None
 * Returns: None
 * Alters: None
 * Implemented: 10/22/16
 * Edited:
 *********************************************************************************/
void setAlarm(void);

/********************************************************************************
 * Function name: When the alarm goes off, play the custom ring tone on the SD
 * 					card, if present, or the default tone. Allow the user to snooze
 * Operation:
 * Inputs: None
 * Returns: None
 * Alters: None
 * Implemented: 10/22/16
 * Edited: 05/28/17
 *********************************************************************************/
void alarm(void);

/********************************************************************************
 * Function name: Let the user turn on/off the alarm, set a snooze value and
 * 					switch between 12/24 hours format
 * Operation:
 * Inputs: None
 * Returns: None
 * Alters: None
 * Implemented: 10/22/16
 * Edited: 05/28/17
 *********************************************************************************/
void settings(void);

#endif /* STATES_H_ */
