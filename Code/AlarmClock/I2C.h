/**
 * CE4100
 * Michele Pancani
 *
 * HEADER FILE
 *
 * Description:
 * Implemented: 12/18/16
 * Edited:
 *
 */

#ifndef I2C_H_
#define I2C_H_

#include <avr/io.h>
#include <avr/interrupt.h>

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#include "Display.h"
#include "Delay.h"

#define SCL_DIVISION_FACTOR 5

#define I2C_RTC_ADD 0xD0
#define I2C_DAC_ADD 0xC0

#define I2C_WRITE 0
#define I2C_READ 1

#define TWS 0xF8

/********************************************************************************
 * Function name: i2c_init
 * Operation: Initiate the I2C peripheral
 * Inputs:
 * Returns: None
 * Alters: None
 * Implemented: 12/18/16
 * Edited:
 *********************************************************************************/
void i2c_init(void);

/********************************************************************************
 * Function name: i2c_Write
 * Operation: Write a byte to the specified register address of the specified slave
 * Inputs: address	8-bit address of the slave device
 * 		   reg		Destination register address
 * 		   data		8-bit data
 * Returns: 0 if transaction was successful, error code otherwise
 * Alters: None
 * Implemented: 12/18/16
 * Edited:
 *********************************************************************************/
uint8_t i2c_Write(uint8_t address, uint8_t reg, uint8_t data);

/********************************************************************************
 * Function name: i2c_Read
 * Operation: Read a byte from the specified register address of the specified slave
 * Inputs: address	8-bit address of the slave device
 * 		   reg		Source register address
 * Returns: -1 if the transaction failed, data byte otherwise
 * Alters: None
 * Implemented: 12/18/16
 * Edited:
 *********************************************************************************/
uint8_t i2c_Read(uint8_t address, uint8_t reg);

#endif /* I2C_H_ */
