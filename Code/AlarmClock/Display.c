/**
 * CE4100
 * Michele Pancani
 *
 * IMPLEMENTATION FILE
 *
 * Description:
 * Implemented: 10/25/16
 * Edited: 11/11/16
 *
 */

#include "Display.h"

uint8_t* displayBuffer;

bool displayClearChar(uint8_t x, uint8_t y) {

	// Check boundary conditions
	if((x < 0) || (x >= COLS-4)) {
		return false;
	} // End if

	if((y < 0) || (y >= ROWS-6)) {
		return false;
	} // End if

	// Row 0
	displayDrawPixel(x+0, y+0, BLACK);
	displayDrawPixel(x+1, y+0, BLACK);
	displayDrawPixel(x+2, y+0, BLACK);
	displayDrawPixel(x+3, y+0, BLACK);
	displayDrawPixel(x+4, y+0, BLACK);
	// Row 1
	displayDrawPixel(x+0, y+1, BLACK);
	displayDrawPixel(x+1, y+1, BLACK);
	displayDrawPixel(x+2, y+1, BLACK);
	displayDrawPixel(x+3, y+1, BLACK);
	displayDrawPixel(x+4, y+1, BLACK);
	// Row 2
	displayDrawPixel(x+0, y+2, BLACK);
	displayDrawPixel(x+1, y+2, BLACK);
	displayDrawPixel(x+2, y+2, BLACK);
	displayDrawPixel(x+3, y+2, BLACK);
	displayDrawPixel(x+4, y+2, BLACK);
	// Row 3
	displayDrawPixel(x+0, y+3, BLACK);
	displayDrawPixel(x+1, y+3, BLACK);
	displayDrawPixel(x+2, y+3, BLACK);
	displayDrawPixel(x+3, y+3, BLACK);
	displayDrawPixel(x+4, y+3, BLACK);
	// Row 4
	displayDrawPixel(x+0, y+4, BLACK);
	displayDrawPixel(x+1, y+4, BLACK);
	displayDrawPixel(x+2, y+4, BLACK);
	displayDrawPixel(x+3, y+4, BLACK);
	displayDrawPixel(x+4, y+4, BLACK);
	// Row 5
	displayDrawPixel(x+0, y+5, BLACK);
	displayDrawPixel(x+1, y+5, BLACK);
	displayDrawPixel(x+2, y+5, BLACK);
	displayDrawPixel(x+3, y+5, BLACK);
	displayDrawPixel(x+4, y+5, BLACK);
	// Row 6
	displayDrawPixel(x+0, y+6, BLACK);
	displayDrawPixel(x+1, y+6, BLACK);
	displayDrawPixel(x+2, y+6, BLACK);
	displayDrawPixel(x+3, y+6, BLACK);
	displayDrawPixel(x+4, y+6, BLACK);

	return true;

}

void displayClear(void) {

	// Clear buffer
	for(uint16_t i=0; i<(ROWS*COLS); i++) {
		*(displayBuffer+i) = 0x00;
	} // End for loop

}

void displayDispDate(struct dateStruct *date) {

	if(date == NULL) {
		return;
	} // End if

	// Check boundary conditions
	if((date->month < 0) || (date->month > 12)) {
		return;
	} // End if

	if((date->day < 0) || (date->day > 31)) {
		return;
	} // End if

	// Variables
	uint8_t day_tens;
	uint8_t day_units;

	extern uint8_t settingsDateColor;

	// Convert day into ASCII
	day_tens = date->day/10;
	day_units = date->day - day_tens*10 + '0';
	day_tens += '0';

	// Display Date on LED display
	displayDrawChar('-', 17, 9, settingsDateColor);

	switch(date->month) {
		case JAN:
			displayDrawChar('J', 0, 9, settingsDateColor);
			displayDrawChar('A', 6, 9, settingsDateColor);
			displayDrawChar('N', 12, 9, settingsDateColor);
			break;

		case FEB:
			displayDrawChar('F', 0, 9, settingsDateColor);
			displayDrawChar('E', 6, 9, settingsDateColor);
			displayDrawChar('B', 12, 9, settingsDateColor);
			break;

		case MAR:
			displayDrawChar('M', 0, 9, settingsDateColor);
			displayDrawChar('A', 6, 9, settingsDateColor);
			displayDrawChar('R', 12, 9, settingsDateColor);
			break;

		case APR:
			displayDrawChar('A', 0, 9, settingsDateColor);
			displayDrawChar('P', 6, 9, settingsDateColor);
			displayDrawChar('R', 12, 9, settingsDateColor);
			break;

		case MAY:
			displayDrawChar('M', 0, 9, settingsDateColor);
			displayDrawChar('A', 6, 9, settingsDateColor);
			displayDrawChar('Y', 12, 9, settingsDateColor);
			break;

		case JUN:
			displayDrawChar('J', 0, 9, settingsDateColor);
			displayDrawChar('U', 6, 9, settingsDateColor);
			displayDrawChar('N', 12, 9, settingsDateColor);
			break;

		case JUL:
			displayDrawChar('J', 0, 9, settingsDateColor);
			displayDrawChar('U', 6, 9, settingsDateColor);
			displayDrawChar('L', 12, 9, settingsDateColor);
			break;

		case AUG:
			displayDrawChar('A', 0, 9, settingsDateColor);
			displayDrawChar('U', 6, 9, settingsDateColor);
			displayDrawChar('G', 12, 9, settingsDateColor);
			break;

		case SEP:
			displayDrawChar('S', 0, 9, settingsDateColor);
			displayDrawChar('E', 6, 9, settingsDateColor);
			displayDrawChar('P', 12, 9, settingsDateColor);
			break;

		case OCT:
			displayDrawChar('O', 0, 9, settingsDateColor);
			displayDrawChar('C', 6, 9, settingsDateColor);
			displayDrawChar('T', 12, 9, settingsDateColor);
			break;

		case NOV:
			displayDrawChar('N', 0, 9, settingsDateColor);
			displayDrawChar('O', 6, 9, settingsDateColor);
			displayDrawChar('V', 12, 9, settingsDateColor);
			break;

		case DEC:
			displayDrawChar('D', 0, 9, settingsDateColor);
			displayDrawChar('E', 6, 9, settingsDateColor);
			displayDrawChar('C', 12, 9, settingsDateColor);
			break;

		default:
			return;

	} // End switch statement

	displayDrawChar(day_tens, 21, 9, settingsDateColor);
	displayDrawChar(day_units, 27, 9, settingsDateColor);

}

void displayInit(void) {

	// Allocate video memory buffer (16x32)
	displayBuffer = (uint8_t*) malloc(ROWS*COLS);

	// Set Direction registers to output
	DDRB |= ((1<<DISP_OE)|(1<<DISP_CLK));						// B1..0 OE,CLK
	DDRC |= ((1<<DISP_LAT)|(1<<DISP_C)|(1<<DISP_B)|(1<<DISP_A));// C5..2 LAT,C,B,A
	DDRD |= ((1<<DISP_B2)|(1<<DISP_G2)|(1<<DISP_R2)|
			(1<<DISP_B1)|(1<<DISP_G1)|(1<<DISP_R1));			// D7..2 RGB signals

	// Clear buffer
	for(uint16_t i=0; i<(ROWS*COLS); i++) {
		*(displayBuffer+i) = 0x00;
	} // End for loop

}

void displayTerminate(void) {

	// Disable interrupts
	displayInterruptDisable();

	// Turn off display (OE is active low)
	PORTB |= (1<<DISP_OE);

	// Free buffer
	free(displayBuffer);

}

void displayDispLeftArrow(bool display) {

	uint8_t color;
	extern uint8_t settingsSettingsValueColor;

	if(display) {
		color = settingsSettingsValueColor;
	} else {
		color = BLACK;
	} // End if-else

	// Row 0
	displayDrawPixel(2, 10, color);

	// Row 1
	displayDrawPixel(2, 11, color);
	displayDrawPixel(1, 11, color);

	// Row 2
	displayDrawPixel(2, 12, color);
	displayDrawPixel(1, 12, color);
	displayDrawPixel(0, 12, color);

	// Row 3
	displayDrawPixel(2, 13, color);
	displayDrawPixel(1, 13, color);

	// Row 4
	displayDrawPixel(2, 14, color);

}

void displayDispRightArrow(bool display) {

	uint8_t color;
	extern uint8_t settingsSettingsValueColor;

	if(display) {
		color = settingsSettingsValueColor;
	} else {
		color = BLACK;
	} // End if-else

	// Row 0
	displayDrawPixel(29, 10, color);

	// Row 1
	displayDrawPixel(29, 11, color);
	displayDrawPixel(30, 11, color);

	// Row 2
	displayDrawPixel(29, 12, color);
	displayDrawPixel(30, 12, color);
	displayDrawPixel(31, 12, color);

	// Row 3
	displayDrawPixel(29, 13, color);
	displayDrawPixel(30, 13, color);

	// Row 4
	displayDrawPixel(29, 14, color);

}

void displayDispSettingsName(char *str) {

	extern uint8_t settingsSettingsNameColor;

	// Characters counter
	uint8_t counter = 0;

	// Count characters in string
	if(str == NULL) {
		return;
	} else {
		while(str[counter] != 0x00) {
			counter++;
		} // End while loop
	} // End if

	if(counter > MAX_CHARS_PER_ROW) {
		return;
	} // End if

	// Print string and column afterwards
	displayPrintString(str, 0, 0, settingsSettingsNameColor);
	displayDrawPixel(counter*(COLS_PER_CHAR+1), 2, settingsSettingsNameColor);
	displayDrawPixel(counter*(COLS_PER_CHAR+1), 4, settingsSettingsNameColor);

}

void displayDispSettingsValue(uint8_t val, uint8_t format) {

	extern uint8_t settingsSettingsValueColor;
	char string[4];
	uint8_t x, y;
	x = 0;
	y = 9;

	switch(format) {
		case SETTINGS_VAL_FORMAT_PCTG:

			if(val > 100) {

			} // End if

			if(val == 100) {

				displayDrawChar('1', 4, 9, settingsSettingsValueColor);
				displayDrawChar('0', 10, 9, settingsSettingsValueColor);
				displayDrawChar('0', 16, 9, settingsSettingsValueColor);
				displayDrawChar('%', 23, 9, settingsSettingsValueColor);

			} else {

				uint8_t val_tens, val_units;

				// Convert hours and mins into ASCII
				val_tens = val/10;
				val_units = (val- val_tens*10) + '0';
				val_tens += '0';

				displayDrawChar(val_tens, 7, 9, settingsSettingsValueColor);
				displayDrawChar(val_units, 13, 9, settingsSettingsValueColor);
				displayDrawChar('%', 20, 9, settingsSettingsValueColor);

			} // End if

			break;

		case SETTINGS_VAL_FORMAT_HEX:

			break;

		case SETTINGS_VAL_FORMAT_DEG:

			if(val < 100) {
				// Convert tens and units into ASCII
				uint8_t val_tens, val_units;
				val_tens = val/10;
				val_units = (val- val_tens*10) + '0';
				val_tens += '0';

				displayDrawChar(val_tens, 7, 9, settingsSettingsValueColor);
				displayDrawChar(val_units, 13, 9, settingsSettingsValueColor);
				displayDrawChar('�', 20, 9, settingsSettingsValueColor);
			} // End if

			break;

		case SETTINGS_VAL_FORMAT_STRING:

			if(val == SETTINGS_VAL_STRING_ON) {
				string[0] = 'O';
				string[1] = 'N';
				string[2] = 0;
				x = 10;
			} else if(val == SETTINGS_VAL_STRING_OFF) {
				string[0] = 'O';
				string[1] = 'F';
				string[2] = 'F';
				string[3] = 0;
				x = 7;
			} else if(val == SETTINGS_VAL_STRING_AMPM) {
				string[0] = 'H';
				string[1] = '1';
				string[2] = '2';
				string[3] = 0;
				x = 7;
			} else if(val == SETTINGS_VAL_STRING_H24) {
				string[0] = 'H';
				string[1] = '2';
				string[2] = '4';
				string[3] = 0;
				x = 7;
			} else {
				break;
			} // End if-else

			displayPrintString(string, x, y, settingsSettingsValueColor);

			break;

		case SETTINGS_VAL_FORMAT_MINS:

			if(val < 100) {
				// Convert tens and units into ASCII
				uint8_t val_tens, val_units;
				val_tens = val/10;
				val_units = (val- val_tens*10) + '0';
				val_tens += '0';

				displayDrawChar(val_tens, 7, 9, settingsSettingsValueColor);
				displayDrawChar(val_units, 13, 9, settingsSettingsValueColor);
				displayDrawChar('M', 20, 9, settingsSettingsValueColor);
			} // End if

			break;

		default:

			break;

	} // End switch case

}

void displayDispTime(struct timeStruct *time, bool columnOn) {

	if(time == NULL) {
		return;
	} // End if

	uint8_t hours = time->hours;
	uint8_t mins = time->minutes;
	uint8_t am_pm = time->am_pm;

	// Check boundary conditions
	if((hours < 0) || (hours > 24)) {
		return;
	} // End if

	if((mins < 0) || (mins > 60)) {
		return;
	} // End if

	if((am_pm < 0) || (am_pm > 2)) {
		return;
	} // End if

	// Variables
	uint8_t hours_tens;
	uint8_t hours_units;
	uint8_t mins_tens;
	uint8_t mins_units;
	uint8_t xOffset = 0;
	uint8_t yOffset = 0;
	uint8_t timeColor;

	extern uint8_t settingsTimeColor;
	extern uint8_t settingsSettingsValueColor;

	if(time->reg_offset == RTC_ALARM1_TIME_OFFSET) {
		timeColor = settingsSettingsValueColor;
		yOffset = 9;
	} else {
		timeColor = settingsTimeColor;
	} // End if-else

	// If hours are given in 24-hour format and AM_PM is on, convert to 12-hour
	if((am_pm != H_24) && (hours > 12)) {
		hours -= 12;
	} // End if

	// Convert hours and mins into ASCII
	hours_tens = hours/10;
	hours_units = hours - hours_tens*10 + '0';
	hours_tens += '0';

	mins_tens = mins/10;
	mins_units = mins - mins_tens*10 + '0';
	mins_tens += '0';

	// Display Time on LED display
	if(am_pm == AM) {
		displayDrawChar('a', 27, 0+yOffset, timeColor);
	} else if(am_pm == PM) {
		displayDrawChar('p', 27, 0+yOffset, timeColor);
	} else {
		//displayDispItalianFlag(26, 0);
		xOffset = 3;
	} // End if

	if(columnOn) {
		displayDrawChar(':', 10+xOffset, 0+yOffset, timeColor);
	} else {
		displayDrawChar(' ', 10+xOffset, 0+yOffset, timeColor);
	} // End if

	displayDrawChar(hours_tens, 0+xOffset, 0+yOffset, timeColor);
	displayDrawChar(hours_units, 6+xOffset, 0+yOffset, timeColor);
	displayDrawChar(mins_tens, 14+xOffset, 0+yOffset, timeColor);
	displayDrawChar(mins_units, 20+xOffset, 0+yOffset, timeColor);

}

bool displayDrawCursor(uint8_t screen, uint8_t cursor) {

	// Check boundary conditions
	if(cursor > 10) {
		return false;
	} // End if

	if((screen != SCREEN_SET_TIME) && (screen != SCREEN_SET_ALARM)) {
		return false;
	} // End if

	if((screen == SCREEN_SET_ALARM) && (cursor > 4)) {
		return false;
	} // End if

	uint8_t xOffset = 0;
	uint8_t yOffset = 0;
	uint8_t cursor_start;
	uint8_t cursor_end;
	uint8_t cursor_row;
	uint8_t cursor_color;
	uint8_t am_pm;

	extern uint8_t settingsTimeColor;
	extern uint8_t settingsDateColor;
	extern uint8_t settingsSettingsValueColor;
	extern struct timeStruct *currentTime;

	am_pm = currentTime->am_pm;

	// Clear previous cursors
	for(int i=0; i<COLS; i++) {
		displayDrawPixel(i, 7, BLACK);
		displayDrawPixel(i, 8, BLACK);
	} // End for loop

	// Determine parameters to draw new cursor
	switch(cursor) {

		case 0:
			cursor_start = 0;
			cursor_end = 4;
			cursor_row = 7;
			cursor_color = settingsTimeColor;
			// Set xOffset
			if(am_pm == H_24) {
				xOffset = 3;
			} // End if
			break;
		case 1:
			cursor_start = 6;
			cursor_end = 10;
			cursor_row = 7;
			cursor_color = settingsTimeColor;
			// Set xOffset
			if(am_pm == H_24) {
				xOffset = 3;
			} // End if
			break;
		case 2:
			cursor_start = 14;
			cursor_end = 18;
			cursor_row = 7;
			cursor_color = settingsTimeColor;
			// Set xOffset
			if(am_pm == H_24) {
				xOffset = 3;
			} // End if
			break;
		case 3:
			cursor_start = 20;
			cursor_end = 24;
			cursor_row = 7;
			cursor_color = settingsTimeColor;
			// Set xOffset
			if(am_pm == H_24) {
				xOffset = 3;
			} // End if
			break;
		case 4:
			if(am_pm == H_24) {
				return false;
			} // End if
			cursor_start = 26;
			cursor_end = 30;
			cursor_row = 7;
			cursor_color = settingsTimeColor;
			// Set xOffset
			break;
		case 5:
			cursor_start = 0;
			cursor_end = 16;
			cursor_row = 8;
			cursor_color = settingsDateColor;
			break;
		case 6:
			cursor_start = 21;
			cursor_end = 25;
			cursor_row = 8;
			cursor_color = settingsDateColor;
			break;
		case 7:
			cursor_start = 27;
			cursor_end = 31;
			cursor_row = 8;
			cursor_color = settingsDateColor;
			break;
		case 8:
			cursor_start = 16;
			cursor_end = 20;
			cursor_row = 7;
			cursor_color = settingsDateColor;
			break;
		case 9:
			cursor_start = 22;
			cursor_end = 26;
			cursor_row = 7;
			cursor_color = settingsDateColor;
			break;
		case 10:
			cursor_start = 0;
			cursor_end = 31;
			cursor_row = 8;
			cursor_color = settingsDateColor;
			break;

		default:
			return false;
			break;

	} // End switch statement

	if(screen == SCREEN_SET_ALARM) {
		yOffset = 1;
		cursor_color = settingsSettingsValueColor;
	} // End if

	// Draw new cursor
	for(int i=cursor_start; i<=cursor_end; i++) {
		displayDrawPixel(i+xOffset, cursor_row+yOffset, cursor_color);
	} // End for loop

	return true;
}

bool displayDrawChar(uint8_t ascii, uint8_t x, uint8_t y, uint8_t color) {

	// Check boundary conditions
	if((x < 0) || (x >= COLS-4)) {
		return false;
	} // End if

	if((y < 0) || (y >= ROWS-6)) {
		return false;
	} // End if

	switch(ascii) {
		case '0':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+1, y+0, color);
			displayDrawPixel(x+2, y+0, color);
			displayDrawPixel(x+3, y+0, color);
			// Row 1
			displayDrawPixel(x+0, y+1, color);
			displayDrawPixel(x+4, y+1, color);
			// Row 2
			displayDrawPixel(x+0, y+2, color);
			displayDrawPixel(x+3, y+2, color);
			displayDrawPixel(x+4, y+2, color);
			// Row 3
			displayDrawPixel(x+0, y+3, color);
			displayDrawPixel(x+2, y+3, color);
			displayDrawPixel(x+4, y+3, color);
			// Row 4
			displayDrawPixel(x+0, y+4, color);
			displayDrawPixel(x+1, y+4, color);
			displayDrawPixel(x+4, y+4, color);
			// Row 5
			displayDrawPixel(x+0, y+5, color);
			displayDrawPixel(x+4, y+5, color);
			// Row 6
			displayDrawPixel(x+1, y+6, color);
			displayDrawPixel(x+2, y+6, color);
			displayDrawPixel(x+3, y+6, color);

			return true;
			break;

		case '1':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+2, y+0, color);
			displayDrawPixel(x+3, y+0, color);
			// Row 1
			displayDrawPixel(x+1, y+1, color);
			displayDrawPixel(x+3, y+1, color);
			// Row 2
			displayDrawPixel(x+3, y+2, color);
			// Row 3
			displayDrawPixel(x+3, y+3, color);
			// Row 4
			displayDrawPixel(x+3, y+4, color);
			// Row 5
			displayDrawPixel(x+3, y+5, color);
			// Row 6
			displayDrawPixel(x+3, y+6, color);

			return true;
			break;

		case '2':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+1, y+0, color);
			displayDrawPixel(x+2, y+0, color);
			displayDrawPixel(x+3, y+0, color);
			// Row 1
			displayDrawPixel(x+0, y+1, color);
			displayDrawPixel(x+4, y+1, color);
			// Row 2
			displayDrawPixel(x+4, y+2, color);
			// Row 3
			displayDrawPixel(x+4, y+3, color);
			// Row 4
			displayDrawPixel(x+3, y+4, color);
			// Row 5
			displayDrawPixel(x+2, y+5, color);
			// Row 6
			displayDrawPixel(x+0, y+6, color);
			displayDrawPixel(x+1, y+6, color);
			displayDrawPixel(x+2, y+6, color);
			displayDrawPixel(x+3, y+6, color);
			displayDrawPixel(x+4, y+6, color);

			return true;
			break;

		case '3':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+1, y+0, color);
			displayDrawPixel(x+2, y+0, color);
			displayDrawPixel(x+3, y+0, color);
			// Row 1
			displayDrawPixel(x+0, y+1, color);
			displayDrawPixel(x+4, y+1, color);
			// Row 2
			displayDrawPixel(x+4, y+2, color);
			// Row 3
			displayDrawPixel(x+1, y+3, color);
			displayDrawPixel(x+2, y+3, color);
			displayDrawPixel(x+3, y+3, color);
			// Row 4
			displayDrawPixel(x+4, y+4, color);
			// Row 5
			displayDrawPixel(x+0, y+5, color);
			displayDrawPixel(x+4, y+5, color);
			// Row 6
			displayDrawPixel(x+1, y+6, color);
			displayDrawPixel(x+2, y+6, color);
			displayDrawPixel(x+3, y+6, color);

			return true;
			break;

		case '4':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+4, y+0, color);
			// Row 1
			displayDrawPixel(x+3, y+1, color);
			displayDrawPixel(x+4, y+1, color);
			// Row 2
			displayDrawPixel(x+2, y+2, color);
			displayDrawPixel(x+4, y+2, color);
			// Row 3
			displayDrawPixel(x+1, y+3, color);
			displayDrawPixel(x+4, y+3, color);
			// Row 4
			displayDrawPixel(x+0, y+4, color);
			displayDrawPixel(x+1, y+4, color);
			displayDrawPixel(x+2, y+4, color);
			displayDrawPixel(x+3, y+4, color);
			displayDrawPixel(x+4, y+4, color);
			// Row 5
			displayDrawPixel(x+4, y+5, color);
			// Row 6
			displayDrawPixel(x+4, y+6, color);

			return true;
			break;

		case '5':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+0, y+0, color);
			displayDrawPixel(x+1, y+0, color);
			displayDrawPixel(x+2, y+0, color);
			displayDrawPixel(x+3, y+0, color);
			displayDrawPixel(x+4, y+0, color);
			// Row 1
			displayDrawPixel(x+0, y+1, color);
			// Row 2
			displayDrawPixel(x+0, y+2, color);
			// Row 3
			displayDrawPixel(x+1, y+3, color);
			displayDrawPixel(x+2, y+3, color);
			displayDrawPixel(x+3, y+3, color);
			// Row 4
			displayDrawPixel(x+4, y+4, color);
			// Row 5
			displayDrawPixel(x+0, y+5, color);
			displayDrawPixel(x+4, y+5, color);
			// Row 6
			displayDrawPixel(x+1, y+6, color);
			displayDrawPixel(x+2, y+6, color);
			displayDrawPixel(x+3, y+6, color);

			return true;
			break;

		case '6':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+1, y+0, color);
			displayDrawPixel(x+2, y+0, color);
			displayDrawPixel(x+3, y+0, color);
			// Row 1
			displayDrawPixel(x+0, y+1, color);
			displayDrawPixel(x+4, y+1, color);
			// Row 2
			displayDrawPixel(x+0, y+2, color);
			// Row 3
			displayDrawPixel(x+0, y+3, color);
			displayDrawPixel(x+1, y+3, color);
			displayDrawPixel(x+2, y+3, color);
			displayDrawPixel(x+3, y+3, color);
			// Row 4
			displayDrawPixel(x+0, y+4, color);
			displayDrawPixel(x+4, y+4, color);
			// Row 5
			displayDrawPixel(x+0, y+5, color);
			displayDrawPixel(x+4, y+5, color);
			// Row 6
			displayDrawPixel(x+1, y+6, color);
			displayDrawPixel(x+2, y+6, color);
			displayDrawPixel(x+3, y+6, color);

			return true;
			break;

		case '7':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+0, y+0, color);
			displayDrawPixel(x+1, y+0, color);
			displayDrawPixel(x+2, y+0, color);
			displayDrawPixel(x+3, y+0, color);
			displayDrawPixel(x+4, y+0, color);
			// Row 1
			displayDrawPixel(x+4, y+1, color);
			// Row 2
			displayDrawPixel(x+4, y+2, color);
			// Row 3
			displayDrawPixel(x+3, y+3, color);
			// Row 4
			displayDrawPixel(x+2, y+4, color);
			// Row 5
			displayDrawPixel(x+2, y+5, color);
			// Row 6
			displayDrawPixel(x+2, y+6, color);

			return true;
			break;

		case '8':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+1, y+0, color);
			displayDrawPixel(x+2, y+0, color);
			displayDrawPixel(x+3, y+0, color);
			// Row 1
			displayDrawPixel(x+0, y+1, color);
			displayDrawPixel(x+4, y+1, color);
			// Row 2
			displayDrawPixel(x+0, y+2, color);
			displayDrawPixel(x+4, y+2, color);
			// Row 3
			displayDrawPixel(x+1, y+3, color);
			displayDrawPixel(x+2, y+3, color);
			displayDrawPixel(x+3, y+3, color);
			// Row 4
			displayDrawPixel(x+0, y+4, color);
			displayDrawPixel(x+4, y+4, color);
			// Row 5
			displayDrawPixel(x+0, y+5, color);
			displayDrawPixel(x+4, y+5, color);
			// Row 6
			displayDrawPixel(x+1, y+6, color);
			displayDrawPixel(x+2, y+6, color);
			displayDrawPixel(x+3, y+6, color);

			return true;
			break;

		case '9':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+1, y+0, color);
			displayDrawPixel(x+2, y+0, color);
			displayDrawPixel(x+3, y+0, color);
			// Row 1
			displayDrawPixel(x+0, y+1, color);
			displayDrawPixel(x+4, y+1, color);
			// Row 2
			displayDrawPixel(x+0, y+2, color);
			displayDrawPixel(x+4, y+2, color);
			// Row 3
			displayDrawPixel(x+1, y+3, color);
			displayDrawPixel(x+2, y+3, color);
			displayDrawPixel(x+3, y+3, color);
			displayDrawPixel(x+4, y+3, color);
			// Row 4
			displayDrawPixel(x+4, y+4, color);
			// Row 5
			displayDrawPixel(x+0, y+5, color);
			displayDrawPixel(x+4, y+5, color);
			// Row 6
			displayDrawPixel(x+1, y+6, color);
			displayDrawPixel(x+2, y+6, color);
			displayDrawPixel(x+3, y+6, color);

			return true;
			break;

		case ' ':
			displayClearChar(x, y);
			return displayClearChar(x, y);
			break;

		case ':':
			displayClearChar(x, y);

			// Row 0
			// Row 1
			// Row 2
			displayDrawPixel(x+2, y+2, color);
			// Row 3
			// Row 4
			displayDrawPixel(x+2, y+4, color);
			// Row 5
			// Row 6

			return true;
			break;

		case '-':
			displayClearChar(x, y);

			// Row 0
			// Row 1
			// Row 2
			// Row 3
			displayDrawPixel(x+1, y+3, color);
			displayDrawPixel(x+2, y+3, color);
			// Row 4
			// Row 5
			// Row 6

			return true;
			break;

		case 'x':
			displayClearChar(x, y);

			// Row 0
			// Row 1
			displayDrawPixel(x+0, y+1, color);
			displayDrawPixel(x+3, y+1, color);
			// Row 2
			displayDrawPixel(x+0, y+2, color);
			displayDrawPixel(x+3, y+2, color);
			// Row 3
			displayDrawPixel(x+1, y+3, color);
			displayDrawPixel(x+2, y+3, color);
			// Row 4
			displayDrawPixel(x+0, y+4, color);
			displayDrawPixel(x+3, y+4, color);
			// Row 5
			displayDrawPixel(x+0, y+5, color);
			displayDrawPixel(x+3, y+5, color);
			// Row 6

			return true;
			break;

		case '%':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+0, y+0, color);
			displayDrawPixel(x+1, y+0, color);
			// Row 1
			displayDrawPixel(x+0, y+1, color);
			displayDrawPixel(x+1, y+1, color);
			displayDrawPixel(x+4, y+1, color);
			// Row 2
			displayDrawPixel(x+3, y+2, color);
			// Row 3
			displayDrawPixel(x+2, y+3, color);
			// Row 4
			displayDrawPixel(x+1, y+4, color);
			// Row 5
			displayDrawPixel(x+0, y+5, color);
			displayDrawPixel(x+3, y+5, color);
			displayDrawPixel(x+4, y+5, color);
			// Row 6
			displayDrawPixel(x+3, y+6, color);
			displayDrawPixel(x+4, y+6, color);

			return true;
			break;

		case '�':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+1, y+0, color);
			displayDrawPixel(x+2, y+0, color);
			displayDrawPixel(x+3, y+0, color);
			// Row 1
			displayDrawPixel(x+1, y+1, color);
			displayDrawPixel(x+3, y+1, color);
			// Row 2
			displayDrawPixel(x+1, y+2, color);
			displayDrawPixel(x+2, y+2, color);
			displayDrawPixel(x+3, y+2, color);
			// Row 3
			// Row 4
			// Row 5
			// Row 6

			return true;
			break;

		case 'a':
			displayClearChar(x, y);

			// Row 0
			// Row 1
			displayDrawPixel(x+1, y+1, color);
			displayDrawPixel(x+2, y+1, color);
			displayDrawPixel(x+3, y+1, color);
			// Row 2
			displayDrawPixel(x+0, y+2, color);
			displayDrawPixel(x+4, y+2, color);
			// Row 3
			displayDrawPixel(x+0, y+3, color);
			displayDrawPixel(x+4, y+3, color);
			// Row 4
			displayDrawPixel(x+0, y+4, color);
			displayDrawPixel(x+1, y+4, color);
			displayDrawPixel(x+2, y+4, color);
			displayDrawPixel(x+3, y+4, color);
			displayDrawPixel(x+4, y+4, color);
			// Row 5
			displayDrawPixel(x+0, y+5, color);
			displayDrawPixel(x+4, y+5, color);
			// Row 6
			displayDrawPixel(x+0, y+6, color);
			displayDrawPixel(x+4, y+6, color);

			return true;
			break;

		case 'p':
			displayClearChar(x, y);

			// Row 0
			// Row 1
			displayDrawPixel(x+0, y+1, color);
			displayDrawPixel(x+1, y+1, color);
			displayDrawPixel(x+2, y+1, color);
			// Row 2
			displayDrawPixel(x+0, y+2, color);
			displayDrawPixel(x+3, y+2, color);
			// Row 3
			displayDrawPixel(x+0, y+3, color);
			displayDrawPixel(x+3, y+3, color);
			// Row 4
			displayDrawPixel(x+0, y+4, color);
			displayDrawPixel(x+1, y+4, color);
			displayDrawPixel(x+2, y+4, color);
			// Row 5
			displayDrawPixel(x+0, y+5, color);
			// Row 6
			displayDrawPixel(x+0, y+6, color);

			return true;
			break;

		case 'A':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+1, y+0, color);
			displayDrawPixel(x+2, y+0, color);
			displayDrawPixel(x+3, y+0, color);
			// Row 1
			displayDrawPixel(x+0, y+1, color);
			displayDrawPixel(x+4, y+1, color);
			// Row 2
			displayDrawPixel(x+0, y+2, color);
			displayDrawPixel(x+4, y+2, color);
			// Row 3
			displayDrawPixel(x+0, y+3, color);
			displayDrawPixel(x+1, y+3, color);
			displayDrawPixel(x+2, y+3, color);
			displayDrawPixel(x+3, y+3, color);
			displayDrawPixel(x+4, y+3, color);
			// Row 4
			displayDrawPixel(x+0, y+4, color);
			displayDrawPixel(x+4, y+4, color);
			// Row 5
			displayDrawPixel(x+0, y+5, color);
			displayDrawPixel(x+4, y+5, color);
			// Row 6
			displayDrawPixel(x+0, y+6, color);
			displayDrawPixel(x+4, y+6, color);

			return true;
			break;

		case 'B':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+0, y+0, color);
			displayDrawPixel(x+1, y+0, color);
			displayDrawPixel(x+2, y+0, color);
			displayDrawPixel(x+3, y+0, color);
			// Row 1
			displayDrawPixel(x+0, y+1, color);
			displayDrawPixel(x+4, y+1, color);
			// Row 2
			displayDrawPixel(x+0, y+2, color);
			displayDrawPixel(x+4, y+2, color);
			// Row 3
			displayDrawPixel(x+0, y+3, color);
			displayDrawPixel(x+1, y+3, color);
			displayDrawPixel(x+2, y+3, color);
			displayDrawPixel(x+3, y+3, color);
			// Row 4
			displayDrawPixel(x+0, y+4, color);
			displayDrawPixel(x+4, y+4, color);
			// Row 5
			displayDrawPixel(x+0, y+5, color);
			displayDrawPixel(x+4, y+5, color);
			// Row 6
			displayDrawPixel(x+0, y+6, color);
			displayDrawPixel(x+1, y+6, color);
			displayDrawPixel(x+2, y+6, color);
			displayDrawPixel(x+3, y+6, color);

			return true;
			break;

		case 'C':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+1, y+0, color);
			displayDrawPixel(x+2, y+0, color);
			displayDrawPixel(x+3, y+0, color);
			// Row 1
			displayDrawPixel(x+0, y+1, color);
			displayDrawPixel(x+4, y+1, color);
			// Row 2
			displayDrawPixel(x+0, y+2, color);
			// Row 3
			displayDrawPixel(x+0, y+3, color);
			// Row 4
			displayDrawPixel(x+0, y+4, color);
			// Row 5
			displayDrawPixel(x+0, y+5, color);
			displayDrawPixel(x+4, y+5, color);
			// Row 6
			displayDrawPixel(x+1, y+6, color);
			displayDrawPixel(x+2, y+6, color);
			displayDrawPixel(x+3, y+6, color);

			return true;
			break;

		case 'D':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+0, y+0, color);
			displayDrawPixel(x+1, y+0, color);
			displayDrawPixel(x+2, y+0, color);
			displayDrawPixel(x+3, y+0, color);
			// Row 1
			displayDrawPixel(x+0, y+1, color);
			displayDrawPixel(x+4, y+1, color);
			// Row 2
			displayDrawPixel(x+0, y+2, color);
			displayDrawPixel(x+4, y+2, color);
			// Row 3
			displayDrawPixel(x+0, y+3, color);
			displayDrawPixel(x+4, y+3, color);
			// Row 4
			displayDrawPixel(x+0, y+4, color);
			displayDrawPixel(x+4, y+4, color);
			// Row 5
			displayDrawPixel(x+0, y+5, color);
			displayDrawPixel(x+4, y+5, color);
			// Row 6
			displayDrawPixel(x+0, y+6, color);
			displayDrawPixel(x+1, y+6, color);
			displayDrawPixel(x+2, y+6, color);
			displayDrawPixel(x+3, y+6, color);

			return true;
			break;

		case 'E':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+0, y+0, color);
			displayDrawPixel(x+1, y+0, color);
			displayDrawPixel(x+2, y+0, color);
			displayDrawPixel(x+3, y+0, color);
			displayDrawPixel(x+4, y+0, color);
			// Row 1
			displayDrawPixel(x+0, y+1, color);
			// Row 2
			displayDrawPixel(x+0, y+2, color);
			// Row 3
			displayDrawPixel(x+0, y+3, color);
			displayDrawPixel(x+1, y+3, color);
			displayDrawPixel(x+2, y+3, color);
			displayDrawPixel(x+3, y+3, color);
			displayDrawPixel(x+4, y+3, color);
			// Row 4
			displayDrawPixel(x+0, y+4, color);
			// Row 5
			displayDrawPixel(x+0, y+5, color);
			// Row 6
			displayDrawPixel(x+0, y+6, color);
			displayDrawPixel(x+1, y+6, color);
			displayDrawPixel(x+2, y+6, color);
			displayDrawPixel(x+3, y+6, color);
			displayDrawPixel(x+4, y+6, color);

			return true;
			break;

		case 'F':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+0, y+0, color);
			displayDrawPixel(x+1, y+0, color);
			displayDrawPixel(x+2, y+0, color);
			displayDrawPixel(x+3, y+0, color);
			displayDrawPixel(x+4, y+0, color);
			// Row 1
			displayDrawPixel(x+0, y+1, color);
			// Row 2
			displayDrawPixel(x+0, y+2, color);
			// Row 3
			displayDrawPixel(x+0, y+3, color);
			displayDrawPixel(x+1, y+3, color);
			displayDrawPixel(x+2, y+3, color);
			// Row 4
			displayDrawPixel(x+0, y+4, color);
			// Row 5
			displayDrawPixel(x+0, y+5, color);
			// Row 6
			displayDrawPixel(x+0, y+6, color);

			return true;
			break;

		case 'G':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+1, y+0, color);
			displayDrawPixel(x+2, y+0, color);
			displayDrawPixel(x+3, y+0, color);
			// Row 1
			displayDrawPixel(x+0, y+1, color);
			displayDrawPixel(x+4, y+1, color);
			// Row 2
			displayDrawPixel(x+0, y+2, color);
			displayDrawPixel(x+4, y+2, color);
			// Row 3
			displayDrawPixel(x+0, y+3, color);
			// Row 4
			displayDrawPixel(x+0, y+4, color);
			displayDrawPixel(x+2, y+4, color);
			displayDrawPixel(x+3, y+4, color);
			displayDrawPixel(x+4, y+4, color);
			// Row 5
			displayDrawPixel(x+0, y+5, color);
			displayDrawPixel(x+4, y+5, color);
			// Row 6
			displayDrawPixel(x+1, y+6, color);
			displayDrawPixel(x+2, y+6, color);
			displayDrawPixel(x+3, y+6, color);

			return true;
			break;

		case 'H':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+0, y+0, color);
			displayDrawPixel(x+4, y+0, color);
			// Row 1
			displayDrawPixel(x+0, y+1, color);
			displayDrawPixel(x+4, y+1, color);
			// Row 2
			displayDrawPixel(x+0, y+2, color);
			displayDrawPixel(x+4, y+2, color);
			// Row 3
			displayDrawPixel(x+0, y+3, color);
			displayDrawPixel(x+1, y+3, color);
			displayDrawPixel(x+2, y+3, color);
			displayDrawPixel(x+3, y+3, color);
			displayDrawPixel(x+4, y+3, color);
			// Row 4
			displayDrawPixel(x+0, y+4, color);
			displayDrawPixel(x+4, y+4, color);
			// Row 5
			displayDrawPixel(x+0, y+5, color);
			displayDrawPixel(x+4, y+5, color);
			// Row 6
			displayDrawPixel(x+0, y+6, color);
			displayDrawPixel(x+4, y+6, color);

			return true;
			break;

		case 'I':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+2, y+0, color);
			// Row 1
			displayDrawPixel(x+2, y+1, color);
			// Row 2
			displayDrawPixel(x+2, y+2, color);
			// Row 3
			displayDrawPixel(x+2, y+3, color);
			// Row 4
			displayDrawPixel(x+2, y+4, color);
			// Row 5
			displayDrawPixel(x+2, y+5, color);
			// Row 6
			displayDrawPixel(x+2, y+6, color);

			return true;
			break;

		case 'J':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+2, y+0, color);
			// Row 1
			displayDrawPixel(x+2, y+1, color);
			// Row 2
			displayDrawPixel(x+2, y+2, color);
			// Row 3
			displayDrawPixel(x+2, y+3, color);
			// Row 4
			displayDrawPixel(x+2, y+4, color);
			// Row 5
			displayDrawPixel(x+0, y+5, color);
			displayDrawPixel(x+2, y+5, color);
			// Row 6
			displayDrawPixel(x+1, y+6, color);

			return true;
			break;

		case 'K':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+0, y+0, color);
			displayDrawPixel(x+4, y+0, color);
			// Row 1
			displayDrawPixel(x+0, y+1, color);
			displayDrawPixel(x+3, y+1, color);
			// Row 2
			displayDrawPixel(x+0, y+2, color);
			displayDrawPixel(x+2, y+2, color);
			// Row 3
			displayDrawPixel(x+0, y+3, color);
			displayDrawPixel(x+1, y+3, color);
			// Row 4
			displayDrawPixel(x+0, y+4, color);
			displayDrawPixel(x+2, y+4, color);
			// Row 5
			displayDrawPixel(x+0, y+5, color);
			displayDrawPixel(x+3, y+5, color);
			// Row 6
			displayDrawPixel(x+0, y+6, color);
			displayDrawPixel(x+4, y+6, color);

			return true;
			break;

		case 'L':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+0, y+0, color);
			// Row 1
			displayDrawPixel(x+0, y+1, color);
			// Row 2
			displayDrawPixel(x+0, y+2, color);
			// Row 3
			displayDrawPixel(x+0, y+3, color);
			// Row 4
			displayDrawPixel(x+0, y+4, color);
			// Row 5
			displayDrawPixel(x+0, y+5, color);
			// Row 6
			displayDrawPixel(x+0, y+6, color);
			displayDrawPixel(x+1, y+6, color);
			displayDrawPixel(x+2, y+6, color);
			displayDrawPixel(x+3, y+6, color);
			displayDrawPixel(x+4, y+6, color);

			return true;
			break;

		case 'M':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+0, y+0, color);
			displayDrawPixel(x+4, y+0, color);
			// Row 1
			displayDrawPixel(x+0, y+1, color);
			displayDrawPixel(x+1, y+1, color);
			displayDrawPixel(x+3, y+1, color);
			displayDrawPixel(x+4, y+1, color);
			// Row 2
			displayDrawPixel(x+0, y+2, color);
			displayDrawPixel(x+2, y+2, color);
			displayDrawPixel(x+4, y+2, color);
			// Row 3
			displayDrawPixel(x+0, y+3, color);
			displayDrawPixel(x+4, y+3, color);
			// Row 4
			displayDrawPixel(x+0, y+4, color);
			displayDrawPixel(x+4, y+4, color);
			// Row 5
			displayDrawPixel(x+0, y+5, color);
			displayDrawPixel(x+4, y+5, color);
			// Row 6
			displayDrawPixel(x+0, y+6, color);
			displayDrawPixel(x+4, y+6, color);

			return true;
			break;

		case 'N':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+0, y+0, color);
			displayDrawPixel(x+4, y+0, color);
			// Row 1
			displayDrawPixel(x+0, y+1, color);
			displayDrawPixel(x+4, y+1, color);
			// Row 2
			displayDrawPixel(x+0, y+2, color);
			displayDrawPixel(x+1, y+2, color);
			displayDrawPixel(x+4, y+2, color);
			// Row 3
			displayDrawPixel(x+0, y+3, color);
			displayDrawPixel(x+2, y+3, color);
			displayDrawPixel(x+4, y+3, color);
			// Row 4
			displayDrawPixel(x+0, y+4, color);
			displayDrawPixel(x+3, y+4, color);
			displayDrawPixel(x+4, y+4, color);
			// Row 5
			displayDrawPixel(x+0, y+5, color);
			displayDrawPixel(x+4, y+5, color);
			// Row 6
			displayDrawPixel(x+0, y+6, color);
			displayDrawPixel(x+4, y+6, color);

			return true;
			break;

		case 'O':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+1, y+0, color);
			displayDrawPixel(x+2, y+0, color);
			displayDrawPixel(x+3, y+0, color);
			// Row 1
			displayDrawPixel(x+0, y+1, color);
			displayDrawPixel(x+4, y+1, color);
			// Row 2
			displayDrawPixel(x+0, y+2, color);
			displayDrawPixel(x+4, y+2, color);
			// Row 3
			displayDrawPixel(x+0, y+3, color);
			displayDrawPixel(x+4, y+3, color);
			// Row 4
			displayDrawPixel(x+0, y+4, color);
			displayDrawPixel(x+4, y+4, color);
			// Row 5
			displayDrawPixel(x+0, y+5, color);
			displayDrawPixel(x+4, y+5, color);
			// Row 6
			displayDrawPixel(x+1, y+6, color);
			displayDrawPixel(x+2, y+6, color);
			displayDrawPixel(x+3, y+6, color);

			return true;
			break;

		case 'P':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+0, y+0, color);
			displayDrawPixel(x+1, y+0, color);
			displayDrawPixel(x+2, y+0, color);
			displayDrawPixel(x+3, y+0, color);
			// Row 1
			displayDrawPixel(x+0, y+1, color);
			displayDrawPixel(x+4, y+1, color);
			// Row 2
			displayDrawPixel(x+0, y+2, color);
			displayDrawPixel(x+4, y+2, color);
			// Row 3
			displayDrawPixel(x+0, y+3, color);
			displayDrawPixel(x+1, y+3, color);
			displayDrawPixel(x+2, y+3, color);
			displayDrawPixel(x+3, y+3, color);
			// Row 4
			displayDrawPixel(x+0, y+4, color);
			// Row 5
			displayDrawPixel(x+0, y+5, color);
			// Row 6
			displayDrawPixel(x+0, y+6, color);

			return true;
			break;

		case 'Q':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+1, y+0, color);
			displayDrawPixel(x+2, y+0, color);
			displayDrawPixel(x+3, y+0, color);
			// Row 1
			displayDrawPixel(x+0, y+1, color);
			displayDrawPixel(x+4, y+1, color);
			// Row 2
			displayDrawPixel(x+0, y+2, color);
			displayDrawPixel(x+4, y+2, color);
			// Row 3
			displayDrawPixel(x+0, y+3, color);
			displayDrawPixel(x+4, y+3, color);
			// Row 4
			displayDrawPixel(x+0, y+4, color);
			displayDrawPixel(x+2, y+4, color);
			displayDrawPixel(x+4, y+4, color);
			// Row 5
			displayDrawPixel(x+0, y+5, color);
			displayDrawPixel(x+3, y+5, color);
			displayDrawPixel(x+4, y+5, color);
			// Row 6
			displayDrawPixel(x+1, y+6, color);
			displayDrawPixel(x+2, y+6, color);
			displayDrawPixel(x+3, y+6, color);
			displayDrawPixel(x+4, y+6, color);

			return true;
			break;

		case 'R':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+0, y+0, color);
			displayDrawPixel(x+1, y+0, color);
			displayDrawPixel(x+2, y+0, color);
			displayDrawPixel(x+3, y+0, color);
			// Row 1
			displayDrawPixel(x+0, y+1, color);
			displayDrawPixel(x+4, y+1, color);
			// Row 2
			displayDrawPixel(x+0, y+2, color);
			displayDrawPixel(x+4, y+2, color);
			// Row 3
			displayDrawPixel(x+0, y+3, color);
			displayDrawPixel(x+1, y+3, color);
			displayDrawPixel(x+2, y+3, color);
			displayDrawPixel(x+3, y+3, color);
			// Row 4
			displayDrawPixel(x+0, y+4, color);
			displayDrawPixel(x+4, y+4, color);
			// Row 5
			displayDrawPixel(x+0, y+5, color);
			displayDrawPixel(x+4, y+5, color);
			// Row 6
			displayDrawPixel(x+0, y+6, color);
			displayDrawPixel(x+4, y+6, color);

			return true;
			break;

		case 'S':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+1, y+0, color);
			displayDrawPixel(x+2, y+0, color);
			displayDrawPixel(x+3, y+0, color);
			// Row 1
			displayDrawPixel(x+0, y+1, color);
			displayDrawPixel(x+4, y+1, color);
			// Row 2
			displayDrawPixel(x+0, y+2, color);
			// Row 3
			displayDrawPixel(x+1, y+3, color);
			displayDrawPixel(x+2, y+3, color);
			displayDrawPixel(x+3, y+3, color);
			// Row 4
			displayDrawPixel(x+4, y+4, color);
			// Row 5
			displayDrawPixel(x+0, y+5, color);
			displayDrawPixel(x+4, y+5, color);
			// Row 6
			displayDrawPixel(x+1, y+6, color);
			displayDrawPixel(x+2, y+6, color);
			displayDrawPixel(x+3, y+6, color);

			return true;
			break;

		case 'T':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+0, y+0, color);
			displayDrawPixel(x+1, y+0, color);
			displayDrawPixel(x+2, y+0, color);
			displayDrawPixel(x+3, y+0, color);
			displayDrawPixel(x+4, y+0, color);
			// Row 1
			displayDrawPixel(x+2, y+1, color);
			// Row 2
			displayDrawPixel(x+2, y+2, color);
			// Row 3
			displayDrawPixel(x+2, y+3, color);
			// Row 4
			displayDrawPixel(x+2, y+4, color);
			// Row 5
			displayDrawPixel(x+2, y+5, color);
			// Row 6
			displayDrawPixel(x+2, y+6, color);

			return true;
			break;

		case 'U':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+0, y+0, color);
			displayDrawPixel(x+4, y+0, color);
			// Row 1
			displayDrawPixel(x+0, y+1, color);
			displayDrawPixel(x+4, y+1, color);
			// Row 2
			displayDrawPixel(x+0, y+2, color);
			displayDrawPixel(x+4, y+2, color);
			// Row 3
			displayDrawPixel(x+0, y+3, color);
			displayDrawPixel(x+4, y+3, color);
			// Row 4
			displayDrawPixel(x+0, y+4, color);
			displayDrawPixel(x+4, y+4, color);
			// Row 5
			displayDrawPixel(x+0, y+5, color);
			displayDrawPixel(x+4, y+5, color);
			// Row 6
			displayDrawPixel(x+1, y+6, color);
			displayDrawPixel(x+2, y+6, color);
			displayDrawPixel(x+3, y+6, color);

			return true;
			break;

		case 'V':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+0, y+0, color);
			displayDrawPixel(x+4, y+0, color);
			// Row 1
			displayDrawPixel(x+0, y+1, color);
			displayDrawPixel(x+4, y+1, color);
			// Row 2
			displayDrawPixel(x+0, y+2, color);
			displayDrawPixel(x+4, y+2, color);
			// Row 3
			displayDrawPixel(x+0, y+3, color);
			displayDrawPixel(x+4, y+3, color);
			// Row 4
			displayDrawPixel(x+0, y+4, color);
			displayDrawPixel(x+4, y+4, color);
			// Row 5
			displayDrawPixel(x+1, y+5, color);
			displayDrawPixel(x+3, y+5, color);
			// Row 6
			displayDrawPixel(x+2, y+6, color);

			return true;
			break;

		case 'W':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+0, y+0, color);
			displayDrawPixel(x+4, y+0, color);
			// Row 1
			displayDrawPixel(x+0, y+1, color);
			displayDrawPixel(x+4, y+1, color);
			// Row 2
			displayDrawPixel(x+0, y+2, color);
			displayDrawPixel(x+4, y+2, color);
			// Row 3
			displayDrawPixel(x+0, y+3, color);
			displayDrawPixel(x+4, y+3, color);
			// Row 4
			displayDrawPixel(x+0, y+4, color);
			displayDrawPixel(x+4, y+4, color);
			// Row 5
			displayDrawPixel(x+0, y+5, color);
			displayDrawPixel(x+2, y+5, color);
			displayDrawPixel(x+4, y+5, color);
			// Row 6
			displayDrawPixel(x+1, y+6, color);
			displayDrawPixel(x+3, y+6, color);

			return true;
			break;

		case 'X':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+0, y+0, color);
			displayDrawPixel(x+4, y+0, color);
			// Row 1
			displayDrawPixel(x+0, y+1, color);
			displayDrawPixel(x+4, y+1, color);
			// Row 2
			displayDrawPixel(x+1, y+2, color);
			displayDrawPixel(x+3, y+2, color);
			// Row 3
			displayDrawPixel(x+2, y+3, color);
			// Row 4
			displayDrawPixel(x+1, y+4, color);
			displayDrawPixel(x+3, y+4, color);
			// Row 5
			displayDrawPixel(x+0, y+5, color);
			displayDrawPixel(x+4, y+5, color);
			// Row 6
			displayDrawPixel(x+0, y+6, color);
			displayDrawPixel(x+4, y+6, color);

			return true;
			break;

		case 'Y':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+0, y+0, color);
			displayDrawPixel(x+4, y+0, color);
			// Row 1
			displayDrawPixel(x+0, y+1, color);
			displayDrawPixel(x+4, y+1, color);
			// Row 2
			displayDrawPixel(x+0, y+2, color);
			displayDrawPixel(x+4, y+2, color);
			// Row 3
			displayDrawPixel(x+1, y+3, color);
			displayDrawPixel(x+3, y+3, color);
			// Row 4
			displayDrawPixel(x+2, y+4, color);
			// Row 5
			displayDrawPixel(x+2, y+5, color);
			// Row 6
			displayDrawPixel(x+2, y+6, color);

			return true;
			break;

		case 'Z':
			displayClearChar(x, y);

			// Row 0
			displayDrawPixel(x+0, y+0, color);
			displayDrawPixel(x+1, y+0, color);
			displayDrawPixel(x+2, y+0, color);
			displayDrawPixel(x+3, y+0, color);
			displayDrawPixel(x+4, y+0, color);
			// Row 1
			displayDrawPixel(x+4, y+1, color);
			// Row 2
			displayDrawPixel(x+3, y+2, color);
			// Row 3
			displayDrawPixel(x+2, y+3, color);
			// Row 4
			displayDrawPixel(x+1, y+4, color);
			// Row 5
			displayDrawPixel(x+0, y+5, color);
			// Row 6
			displayDrawPixel(x+0, y+6, color);
			displayDrawPixel(x+1, y+6, color);
			displayDrawPixel(x+2, y+6, color);
			displayDrawPixel(x+3, y+6, color);
			displayDrawPixel(x+4, y+6, color);

			return true;
			break;

		default:
			return false;
			break;

	} // End switch case

}

void displayDrawPixel(uint8_t x, uint8_t y, uint8_t color) {

	// Check boundary conditions
	if((x < 0) || (x >= COLS)) {
		return;
	} // End if

	if((y < 0) || (y >= ROWS)) {
		return;
	} // End if

	// Write pixel to video memory buffer
	uint16_t addOffset = x + y*COLS;
	*(displayBuffer+addOffset) = color;

}

void displayDispRefresh(void) {

	// Offset address of pixels to be displayed, respectively,
	// in upper and lower section of the display
	uint16_t addOffset_u = 0;
	uint16_t addOffset_l = 0;

	// Extrapolate RGB info from 8-bit data value
	uint8_t tmpRGB_u = 0;
	uint8_t tmpRGB_l = 0;

	// Extrapolate R, G, B info from tmpRGB value
	uint8_t tmpR1 = 0;
	uint8_t tmpG1 = 0;
	uint8_t tmpB1 = 0;
	uint8_t tmpR2 = 0;
	uint8_t tmpG2 = 0;
	uint8_t tmpB2 = 0;

	// TODO color settings
	uint8_t k = 0;

	// Need to keep track of iterations in order to clock data
	// correctly. Note: Each iteration causes a delay.
	// First iteration (even) sets the clock low and returns.
	// Second iteration sets the data pins and sets the clock high.
	static bool evenIteration = true;

	// Iterate through rows
		// Note: Need to iterate through ROW/2 because
		// each write writes to one row in upper display and
		// one row in lower display simultaneously
	static uint8_t i = 0;
	//for(uint8_t i=0; i<ROWS/2; i++) { // Can't use for loop with interrupts
	if(i<ROWS/2) {


		if(evenIteration) {
			/**
			 * Reset Latch from previous iteration and set new row address
			 * Must disable display while toggling signals
			 * 		Note: OE is active low!
			 */
			PORTB |= ((1<<DISP_OE)|(1<<DISP_CLK));
			PORTC |= (1<<DISP_LAT);
			PORTC &= ~((1<<DISP_C)|(1<<DISP_B)|(1<<DISP_A));
			PORTC |= (i<<DISP_A);
		} // End if

		// Iterate through columns
		static uint8_t j = 0;
		//for(uint16_t j=0; j<COLS; j++) {	// Can't use for loop with interrupts
		if(j<COLS) {

			// Calculate offset of pixels to be written, respectively
			// from upper and lower section of display, and fetch data
			addOffset_u = j + i*COLS;
			addOffset_l = j + (i+ROWS/2)*COLS;
			tmpRGB_u = *(displayBuffer+addOffset_u);
			tmpRGB_l = *(displayBuffer+addOffset_l);

			// Reset clock from previous iteration
			PORTB &= ~(1<<DISP_CLK);
			//delay_us(DISP_CLK_HALF_PERIOD_US);	// Can't use delay with interrupts
			if(evenIteration) {
				evenIteration = false;
				return;
			} // End if

			// Write to port k-th set of RGB bits of upper and lower pixel
			tmpR1 = tmpRGB_u & ((1<<(R*BIT_PER_COLOR))<<k);
				tmpR1 = ((tmpR1 >> (R*BIT_PER_COLOR)) >> k);
			tmpG1 = tmpRGB_u & ((1<<(G*BIT_PER_COLOR))<<k);
				tmpG1 = ((tmpG1 >> (G*BIT_PER_COLOR)) >> k);
			tmpB1 = tmpRGB_u & ((1<<(B*BIT_PER_COLOR))<<k);
				tmpB1 = ((tmpB1 >> (B*BIT_PER_COLOR)) >> k);
			tmpR2 = tmpRGB_l & ((1<<(R*BIT_PER_COLOR))<<k);
				tmpR2 = ((tmpR2 >> (R*BIT_PER_COLOR)) >> k);
			tmpG2 = tmpRGB_l & ((1<<(G*BIT_PER_COLOR))<<k);
				tmpG2 = ((tmpG2 >> (G*BIT_PER_COLOR)) >> k);
			tmpB2 = tmpRGB_l & ((1<<(B*BIT_PER_COLOR))<<k);
				tmpB2 = ((tmpB2 >> (B*BIT_PER_COLOR)) >> k);

			PORTD &= ~((1<<DISP_R1)|(1<<DISP_G1)|(1<<DISP_B1)|(1<<DISP_R2)|(1<<DISP_G2)|(1<<DISP_B2));
			PORTD |= ((tmpR1<<DISP_R1)|(tmpG1<<DISP_G1)|(tmpB1<<DISP_B1)|
						(tmpR2<<DISP_R2)|(tmpG2<<DISP_G2)|(tmpB2<<DISP_B2));

			// Clock data with rising edge
			PORTB |= (1<<DISP_CLK);
			//delay_us(DISP_CLK_HALF_PERIOD_US);	// Can't use delay with interrupts
			if(!evenIteration) {
				evenIteration = true;
				j++;
				return;
			} // End if

		} // End inner for loop (j)

		// Latch to indicate end of row and enable display
		PORTC &= ~(1<<DISP_LAT);
		PORTB &= ~(1<<DISP_OE);
		//delay_us(DISP_LAT_DELAY_US);	// Can't use delay with interrupts
		if(evenIteration) {

			displayInterruptSetTime(DISP_LAT_DELAY_US-DISP_CLK_HALF_PERIOD_US);
			evenIteration = false;
			return;

		} else {

			displayInterruptSetTime(DISP_CLK_HALF_PERIOD_US);
			evenIteration = true;
			j = 0;
			i++;
			if(i >= ROWS/2) {
				i = 0;
			} // End inner if-else
			return;

		} // End outer if-else

	} // End outer for loop (i)

}

bool displayDispItalianFlag(uint8_t x, uint8_t y) {

	// Check boundary conditions
	if((x < 0) || (x >= COLS-5)) {
		return false;
	} // End if

	if((y < 0) || (y >= ROWS-5)) {
		return false;
	} // End if

	displayDrawPixel(x+0, y+0, GREEN);
	displayDrawPixel(x+0, y+1, GREEN);
	displayDrawPixel(x+0, y+2, GREEN);
	displayDrawPixel(x+0, y+3, GREEN);
	displayDrawPixel(x+0, y+4, GREEN);
	displayDrawPixel(x+0, y+5, GREEN);
	displayDrawPixel(x+0, y+6, GREEN);
	displayDrawPixel(x+1, y+0, GREEN);
	displayDrawPixel(x+1, y+1, GREEN);
	displayDrawPixel(x+1, y+2, GREEN);
	displayDrawPixel(x+1, y+3, GREEN);
	displayDrawPixel(x+1, y+4, GREEN);
	displayDrawPixel(x+1, y+5, GREEN);
	displayDrawPixel(x+1, y+6, GREEN);

	displayDrawPixel(x+2, y+0, WHITE);
	displayDrawPixel(x+2, y+1, WHITE);
	displayDrawPixel(x+2, y+2, WHITE);
	displayDrawPixel(x+2, y+3, WHITE);
	displayDrawPixel(x+2, y+4, WHITE);
	displayDrawPixel(x+2, y+5, WHITE);
	displayDrawPixel(x+2, y+6, WHITE);
	displayDrawPixel(x+3, y+0, WHITE);
	displayDrawPixel(x+3, y+1, WHITE);
	displayDrawPixel(x+3, y+2, WHITE);
	displayDrawPixel(x+3, y+3, WHITE);
	displayDrawPixel(x+3, y+4, WHITE);
	displayDrawPixel(x+3, y+5, WHITE);
	displayDrawPixel(x+3, y+6, WHITE);

	displayDrawPixel(x+4, y+0, RED);
	displayDrawPixel(x+4, y+1, RED);
	displayDrawPixel(x+4, y+2, RED);
	displayDrawPixel(x+4, y+3, RED);
	displayDrawPixel(x+4, y+4, RED);
	displayDrawPixel(x+4, y+5, RED);
	displayDrawPixel(x+4, y+6, RED);
	displayDrawPixel(x+5, y+0, RED);
	displayDrawPixel(x+5, y+1, RED);
	displayDrawPixel(x+5, y+2, RED);
	displayDrawPixel(x+5, y+3, RED);
	displayDrawPixel(x+5, y+4, RED);
	displayDrawPixel(x+5, y+5, RED);
	displayDrawPixel(x+5, y+6, RED);

	return true;

}

bool displayPrintString(char *ptr, uint8_t x, uint8_t y, uint8_t color) {

	// Characters counter
	uint8_t counter = 0;

	// Count characters in string
	if(ptr == NULL) {
		return false;
	} else {
		while(ptr[counter] != 0x00) {
			counter++;
		} // End while loop
	} // End if

	if(counter > MAX_CHARS_PER_ROW) {
		return false;
	} // End if

	// Check boundary conditions
	if((x < 0) || (x > COLS-(counter*COLS_PER_CHAR+1))) {
		return false;
	} // End if

	if((y < 0) || (y > ROWS-ROWS_PER_CHAR)) {
		return false;
	} // End if

	// Print characters
	for(int i=0; i<counter; i++) {

		if(!displayDrawChar(ptr[i], x+i*(COLS_PER_CHAR+1), y, color)) {
			displayClearChar(x+i*(COLS_PER_CHAR+1), y);
		} // End if

	} // End for loop

	return true;

}

void displayInterruptEnable(uint16_t us) {

	// Don't allow interrupt delay longer than 3.276 ms
		// Timer will overflow!
	if(us > 3276) {
		return;
	} // End if

	// Clear timer
	TCNT1 = 0x00;

	// Set CTC mode
	TCCR1B |= (1<<WGM12);

	// Set OCR with delay value
	OCR1A = (uint16_t) (us * CPU_FREQ_MHZ - 1);

	// Enable timer interrupt
	TIMSK1 |= (1<<OCIE1A);

	// Set Clock with no prescaler (20 MHz) and start timer
	TCCR1B |= (1<<CS10);

}

void displayInterruptDisable(void) {

	// Disable timer interrupt
	TIMSK1 &= ~(1<<OCIE1A);

	// Stop timer
	TCCR1B &= ~(1<<CS12|1<<CS11|1<<CS10);

}

void displayInterruptSetTime(uint16_t us) {

	// Set OCR with delay value
	OCR1A = (uint16_t) (us * CPU_FREQ_MHZ - 1);

}
