################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Delay.c \
../Display.c \
../Driver.c \
../I2C.c \
../Keypad.c \
../LightSensor.c \
../SPI.c \
../States.c \
../TestFile.c 

C_DEPS += \
./Delay.d \
./Display.d \
./Driver.d \
./I2C.d \
./Keypad.d \
./LightSensor.d \
./SPI.d \
./States.d \
./TestFile.d 

OBJS += \
./Delay.o \
./Display.o \
./Driver.o \
./I2C.o \
./Keypad.o \
./LightSensor.o \
./SPI.o \
./States.o \
./TestFile.o 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -g2 -gstabs -O0 -fpack-struct -fshort-enums -ffunction-sections -fdata-sections -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega644p -DF_CPU=20000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


