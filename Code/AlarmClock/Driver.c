/**
 * CE4100
 * Michele Pancani
 *
 * Driver File
 *
 * Description:
 * Implemented: 10/22/16
 * Edited: 05/28/17
 *
 */

#include <avr/io.h>
#include <avr/interrupt.h>

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>

#include "States.h"
#include "Delay.h"
#include "RTC.h"
#include "I2C.h"
#include "AudioPlayer.h"
#include "Keypad.h"
#include "Display.h"

// ************************************
// List Global Variables Here:
// ************************************
STATE currentState;											// Keep track of the current state
struct timeStruct *currentTime;								// Current Time struct
struct timeStruct *alarm1Time;								// Alarm 1 Time struct
struct dateStruct *currentDate;								// Current Date struct
bool gAlarmOn;												// Keep track of whether the alarm is on or off
uint8_t gSnoozeVal;											// Number of minutes used to snooze the alarm
uint8_t settingsTimeColor = WHITE;							// Color of the displayed time
uint8_t settingsTimeColor_snoozePending = LIGHT_BLUE;		// Color of the displayed time while snooze is pending
uint8_t settingsDateColor = RED;							// Color of the displayed date
uint8_t settingsSettingsNameColor = WHITE;					// Color of the Settings' name
uint8_t settingsSettingsValueColor = GREEN;					// Color of the Settings' value
uint8_t settingsVolumePercentage = 50;						// Volume percentage
char keyPadKeyPressed;										// Keep track of the keypad's key being pressed
bool snoozeBarPressed;										// Keep track of whether the snooze bar is being pressed
bool keyPadKeyPressedDelayReset;							// Flag used to break out of blocking delays when a key is pressed
uint16_t gSysClockCountMs = 0;								// Millisecond counter used in non-blocking delays
BOOL gAudioIsPlaying = false;								// Flag used to disable the display while audio is playing to save resources

// ************************************

// State Machine
int main(void) {

	// Start from the Reset state
	currentState = RESET;

	while(true) {

		switch(currentState) {

			case RESET:
				reset();
				currentState = DISPLAY_TIME;
				break;

			case DISPLAY_TIME:
				displayTime();
				// Next state will be determined inside function
				break;

			case SET_TIME:
				setTime();
				currentState = DISPLAY_TIME;
				break;

			case SET_ALARM:
				setAlarm();
				currentState = DISPLAY_TIME;
				break;

			case ALARM:
				alarm();
				currentState = DISPLAY_TIME;
				break;

			case SETTINGS:
				settings();
				currentState = DISPLAY_TIME;
				break;

			default:
				currentState = RESET;

		} // End of switch statement

	} // End of while loop

	displayTerminate();
	free(currentTime);
	free(alarm1Time);
	free(currentDate);

	return 0;

}

// ************************************
// Interrupt Service Routines:
// ************************************

/**
 * Timer 1 CTC Compare Output Vector
 * Run at fixed intervals, this ISR is used to either
 * refresh the display or send an audio sample in the
 * audio circular buffer to the DAC.
 * The display and the Audio system cannot work
 * simultaneously due to speed and memory limitations.
 */
ISR(TIMER1_COMPA_vect) {

	if(!gAudioIsPlaying) {
		displayDispRefresh();
	} else {
		playNextAudioSample_ISR();
	} // End if-else

}

/**
 * Timer 2 CTC Compare Output Vector
 * ISR used for the non-blocking timer
 * used by the SD Card lib
 */
ISR(TIMER2_COMPA_vect) {

	// Decrement System Clock counter
	if(gSysClockCountMs > 0) {
		gSysClockCountMs--;
	} // End if else

}

// ISR for the keypad
// NOTE: if there is deemed more to be interrupted on PCINT0 it will be necessary
// 		 to move this to a separate file containing all interrupts on PCINT0.
// 		 This will cause the keypad ISR to need more advanced implementation.
// https://sites.google.com/site/qeewiki/books/avr-guide/external-interrupts-on-the-atmega328
ISR(PCINT0_vect) {

	// Debounce keys
	delay_ms(KEYPAD_DEBOUNCE_DELAY_MS);
	// Scan keypad
	keyPadKeyPressed = readKeypad();

	// If falling edge, reset delay timers
	if(keyPadKeyPressed != 'N') {
		keyPadKeyPressedDelayReset = true;
	} // End if

	// Clear the cause of the interrupt
	PCIFR |= (1<<PCIF0);

}

/**
 * PCINT1 Pin Change Vector 1 - ISR for the alarm
 */
ISR(PCINT1_vect) {

	// If pin is low and alarm is enabled, move to alarm state
	if((PINB & (1<<PB2)) == 0) {

		// Clear interrupt flag
		clearAlarm1InterruptFlag();

		// Turn off snooze alarm
		alarm2OnOff(false);

		if(gAlarmOn) {
			currentState = ALARM;	// Go to alarm state
		} // End inner if

	} // End outer if

}

/**
 * PCINT2 Pin Change Vector 2 - ISR for the snooze bar
 */
ISR(PCINT2_vect) {

	// Debounce key
	delay_ms(KEYPAD_DEBOUNCE_DELAY_MS);

	// Read pin
	snoozeBarPressed = !((PINC & (1<<PC6)) >> PC6);

	// If falling edge, reset delay timers
	if(snoozeBarPressed) {
		keyPadKeyPressedDelayReset = true;
	} // End if

}
