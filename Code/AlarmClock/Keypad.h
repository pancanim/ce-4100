/**
 * CE4100
 * Nicolas Matschinegg
 *
 * HEADER FILE
 *
 * Description:
 * Implemented: 10/26/16
 * Edited:
 *
 */

#ifndef KEYPAD_H_
#define KEYPAD_H_

#include <avr/io.h>
#include <avr/interrupt.h>

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#include "Delay.h"

#define KEYPAD_DEBOUNCE_DELAY_MS 1

void keypadSetup(void);

char readKeypad(void);

#endif /* KEYPAD_H_ */
