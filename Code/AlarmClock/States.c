/**
 * CE4100
 * Michele Pancani
 *
 * IMPLEMENTATION FILE
 *
 * Description:
 * Implemented: 10/22/16
 * Edited: 05/28/17
 *
 */

#include "States.h"

extern STATE currentState;
extern struct timeStruct *currentTime;
extern struct timeStruct *alarm1Time;
extern struct dateStruct *currentDate;
extern bool gAlarmOn;
extern uint8_t gSnoozeVal;
extern char keyPadKeyPressed;
extern bool snoozeBarPressed;
extern uint8_t settingsVolumePercentage;
extern bool keyPadKeyPressedDelayReset;
extern BOOL gAudioIsPlaying;

/**
 * Temporary flash the value of the setting changed on the screen
 */
static void flashSettingsChange(char* name, uint8_t value, uint8_t format);

/**
 * Set the item at the given position in the given struct to the given value
 */
static bool setItem(struct timeStruct *time, uint8_t value, uint8_t cursor);

/**
 * Set alarm 2 with current time + snooze value to snooze the alarm
 */
static void snoozeAlarm(uint8_t minutes);

/**
 * Toggle alarm 1 between on and off
 */
static void toggleAlarmOnOff(void);

/**
 * Toggle the hour format between 12 and 24 hours
 */
static void toggleHourFormat(void);

void reset(void) {

	// Initialize global variables
	currentTime = (struct timeStruct*) malloc(sizeof(struct timeStruct));
	alarm1Time = (struct timeStruct*) malloc(sizeof(struct timeStruct));
	currentDate = (struct dateStruct*) malloc(sizeof(struct dateStruct));
	currentTime->hours = 0;
	currentTime->minutes = 0;
	currentTime->am_pm = AM;
	currentTime->reg_offset = RTC_SECS_REG;
	alarm1Time->am_pm = AM;
	alarm1Time->hours = 0;
	alarm1Time->minutes = 0;
	alarm1Time->reg_offset = RTC_ALARM1_TIME_OFFSET;
	currentDate->day = 1;
	currentDate->month = 1;
	currentDate->weekDay = SUNDAY;
	currentDate->year = 2017;

	keyPadKeyPressed = 'N';
	snoozeBarPressed = false;
	keyPadKeyPressedDelayReset = false;

	gSnoozeVal = 5;				// Snooze 5 mins by default TODO - Save snooze val in flash
	gAudioIsPlaying = false;

	// Disable JTAG Interface (Set JTD twice within 4 clock cycles for safety purposes)
	MCUCR |= (1<<JTD);
	MCUCR |= (1<<JTD);

	// Initialize DAC
	DACInit();

	// Initialize Display
	displayInit();
	displayInterruptEnable(DISP_CLK_HALF_PERIOD_US);

	// Initialize I2C
	i2c_init();

	// Initialize RTC
	RTCInit();

	// Set SNOOZE BAR as input
	DDRC &= ~(1<<PC6);
	// Enable internal pull-up resistor
	PORTC |= (1<<PC6);
	// Allow SNOOZE BAR to generate interrupt requests on both edges
	PCICR |= (1<<PCIE2);
	// Allow PCINT to trigger interrupts
	PCMSK2 |= (1<<PCINT22);

	// Initialize Keypad and enable interrupts
	keypadSetup();

	// Set alarmOn flag
	gAlarmOn = isAlarm1On();

	// Enable global interrupts
	SREG |= (1<<PIE);

}

void displayTime(void) {

	// Variable needed to make column between hours and
		// minutes blink on the display
	bool halfSecondEdge_flag = true;
	uint8_t temp;

	// Clear the display
	displayClear();

	while(currentState == DISPLAY_TIME) {

		while(currentState == DISPLAY_TIME) {

			// If a key is pressed, break out of the loop and take action
			if(keyPadKeyPressed != 'N' || snoozeBarPressed) {
				break;
			} // End if

			// Update time and make column between hours and
				// minutes blink. Program will break out of delay_ms() if
				// a key is pressed
			updateTimeFromRTC(currentTime);
			updateDateFromRTC(currentDate);
			displayDispTime(currentTime, halfSecondEdge_flag);
			displayDispDate(currentDate);
			halfSecondEdge_flag = !halfSecondEdge_flag;
			delay_ms(500);

		} // End inner while loop

		switch(keyPadKeyPressed) {
			case '1':
			case '2':
			case '3':
			case '4':
			//case '5':
			case '6':
			case '7':
				// TODO play tone, toggle alarm?
				break;

			case '5':	// Trigger Alarm immediately
				keyPadKeyPressed = 'N';
				currentState = ALARM;
				break;

			case '8':	// Volume down
				if(settingsVolumePercentage >= VOLUME_PERCENTAGE_INCREMENT) {
					settingsVolumePercentage -= VOLUME_PERCENTAGE_INCREMENT;
				} else {
					settingsVolumePercentage = 0;
				} // End if
				flashSettingsChange(SETTINGS_VOL, settingsVolumePercentage, SETTINGS_VAL_FORMAT_PCTG);
				break;

			case '9':	// Volume Up
				if(settingsVolumePercentage <= 100-VOLUME_PERCENTAGE_INCREMENT) {
					settingsVolumePercentage += VOLUME_PERCENTAGE_INCREMENT;
				} else {
					settingsVolumePercentage = 100;
				} // End if
				flashSettingsChange(SETTINGS_VOL, settingsVolumePercentage, SETTINGS_VAL_FORMAT_PCTG);
				break;

			case '0':	// Read Temperature
				// Read temperature in Celsius from RTC
					// Note: Temperature is measured on the Circuit board
					// inside of the enclosure, therefore it may be warmer
					// than the actual room temperature, depending on board heating.
				temp = i2c_Read(I2C_RTC_ADD, RTC_TEMPERATURE_MSB_REG);
				flashSettingsChange(SETTINGS_TEMP, temp, SETTINGS_VAL_FORMAT_DEG);
				break;

			case '*':	// Set Alarm
				// Wait to see if key is being held. If so, change state
				delay_ms(KEY_HOLD_DELAY_MS);
				if(keyPadKeyPressed == '*') {
					keyPadKeyPressed = 'N';
					currentState = SET_ALARM;
				} // End if
				break;

			case '#':	// Set Time and Date
				// Wait to see if key is being held. If so, change state
				delay_ms(KEY_HOLD_DELAY_MS);
				if(keyPadKeyPressed == '#') {
					keyPadKeyPressed = 'N';
					currentState = SET_TIME;
				} // End if
				break;

			default:
				break;

		} // End of switch statement

		if(snoozeBarPressed) {	// Go to Settings Menu
			// Wait to see if key is being held. If so, change state
			delay_ms(KEY_HOLD_DELAY_MS);
			if(snoozeBarPressed) {
				snoozeBarPressed = false;
				currentState = SETTINGS;
			} // End inner if
		} // End outer if

	} // End outer while loop

}

void setTime(void) {

	// Variable needed to make hours and
		// minutes blink on the display
	bool halfSecondEdge_flag = true;
	uint8_t cursor = SET_TIME_CURSOR_OFFSET;

	while(!snoozeBarPressed) {

		while(!snoozeBarPressed) {

			// If a key is pressed, break out of the loop and take action
			if(keyPadKeyPressed != 'N' || snoozeBarPressed) {
				break;
			} // End if

			// Update time and make hours and minutes blink.
				// Program will break out of delay_ms() if
				// a key is pressed
			updateTimeFromRTC(currentTime);
			updateDateFromRTC(currentDate);
			if(halfSecondEdge_flag) {
				displayDispTime(currentTime, true);
				displayDispDate(currentDate);
				displayDrawCursor(SCREEN_SET_TIME, cursor);
			} else {
				displayClear();
			} // End if
			halfSecondEdge_flag = !halfSecondEdge_flag;
			delay_ms(500);

		} // End inner while loop

		switch(keyPadKeyPressed) {
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
			case '0':

				// Set item and increment cursor's position
				if(setItem(currentTime, keyPadKeyPressed-'0', cursor) && (cursor < SET_TIME_CURSOR_OFFSET+SET_TIME_CURSOR_SPAN)) {
					cursor++;
					// Skip AM/PM cursor if format is 24H
					if((currentTime->am_pm == H_24) && (cursor == 4)) {
						cursor++;
					} // End if
				} // End if
				break;

			case '*':	// Move cursor Left
				// Decrement cursor's position
				if(cursor > SET_TIME_CURSOR_OFFSET) {
					cursor--;
					// Skip AM/PM cursor if format is 24H
					if((currentTime->am_pm == H_24) && (cursor == 4)) {
						cursor--;
					} // End if
				} else {
					cursor = SET_TIME_CURSOR_OFFSET;
				} // End if-else
				break;

			case '#':	// Move cursor right
				// Increment cursor's position
				if(cursor < SET_TIME_CURSOR_OFFSET+SET_TIME_CURSOR_SPAN) {
					cursor++;
					// Skip AM/PM cursor if format is 24H
					if((currentTime->am_pm == H_24) && (cursor == 4)) {
						cursor++;
					} // End if
				} else {
					cursor = SET_TIME_CURSOR_OFFSET+SET_TIME_CURSOR_SPAN;
				} // End if-else
				break;

			default:
				break;

		} // End outer switch statement

		// Reset key value
		keyPadKeyPressed = 'N';

	} // End outer while loop

	// Reset seconds
	i2c_Write(I2C_RTC_ADD, RTC_SECS_REG, 0x00);

	snoozeBarPressed = false;

}

void setAlarm(void) {

	// Variable needed to make hours and
		// minutes blink on the display
	uint8_t cursor = SET_ALARM_CURSOR_OFFSET;

	while(!snoozeBarPressed) {

		displayClear();
		displayDispSettingsName((char*) "ALRM");
		updateTimeFromRTC(alarm1Time);
		displayDispTime(alarm1Time, true);
		displayDrawCursor(SCREEN_SET_ALARM, cursor);

		// If a key is pressed, break out of the loop and take action
		while(keyPadKeyPressed == 'N' && !snoozeBarPressed);

		switch(keyPadKeyPressed) {
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
			case '0':

				// Set item and increment cursor's position
				if(setItem(alarm1Time, keyPadKeyPressed-'0', cursor) && (cursor < SET_ALARM_CURSOR_OFFSET+SET_ALARM_CURSOR_SPAN)) {
					cursor++;
					// Skip AM/PM cursor if format is 24H
					if((alarm1Time->am_pm == H_24) && (cursor == 4)) {
						cursor--;
					} // End if
				} // End if
				break;

			case '*':	// Move cursor left
				// Decrement cursor's position
				if(cursor > SET_ALARM_CURSOR_OFFSET) {
					cursor--;
					// Skip AM/PM cursor if format is 24H
					if((alarm1Time->am_pm == H_24) && (cursor == 4)) {
						cursor--;
					} // End if
				} else {
					cursor = SET_ALARM_CURSOR_OFFSET;
				} // End if-else
				break;

			case '#':	// Move cursor right
				// Increment cursor's position
				if(cursor < SET_ALARM_CURSOR_OFFSET+SET_ALARM_CURSOR_SPAN) {
					cursor++;
					// Skip AM/PM cursor if format is 24H
					if((alarm1Time->am_pm == H_24) && (cursor == 4)) {
						cursor--;
					} // End if
				} else {
					cursor = SET_ALARM_CURSOR_OFFSET+SET_ALARM_CURSOR_SPAN;
				} // End if-else
				break;

			default:
				break;

		} // End outer switch statement

		// Reset key value
		keyPadKeyPressed = 'N';

	} // End outer while loop

	// Reset seconds
	i2c_Write(I2C_RTC_ADD, RTC_ALARM1_SECS_REG, 0x00);

	snoozeBarPressed = false;

}

void alarm(void) {

	// Save time at which the alarm occurred in order to
		// have a reference and turn off the alarm after a determined timeout value
	uint8_t alarmTimeOff = currentTime->minutes;

	// Play the alarm tone until a key is pressed or until timeout
	while(keyPadKeyPressed == 'N' && !snoozeBarPressed && ((currentTime->minutes-alarmTimeOff) < ALARM_TIMEOUT_MINS)) {

		if(settingsVolumePercentage > 0) {

			displayClear();
			if(!playAudioFile(ALARM_AUDIO_FILE_NAME)) {	// Play audio file from SD card
				// If playAudioFile fails for one of the following reasons, play the default alarm
					// - SD card is missing
					// - SD card is not compatible
					// - Audio File is missing
					// - Audio File is not compatible
					// - SPI read/write errors

				// Play default tone 2 times. 4 times after timeout/2 minutes are elapsed
				playDefaultAlarm();
				delay_ms(800);
				playDefaultAlarm();
				if((currentTime->minutes-alarmTimeOff) > (ALARM_TIMEOUT_MINS/2)) {
					delay_ms(800);
					playDefaultAlarm();
					delay_ms(800);
					playDefaultAlarm();
				} // End if
			} // End if

			// If a key is pressed, break out of the loop and take action
			if(keyPadKeyPressed != 'N' || snoozeBarPressed) {
				break;
			} // End if

		} else {
			delay_ms(1000);
		} // End if

		// If a key is pressed, break out of the loop and take action
		if(keyPadKeyPressed != 'N' || snoozeBarPressed) {
			break;
		} // End if

		// Make date and time blink 5 times between one ring tone and the other
		for(uint8_t i=0; i<5; i++) {
			displayClear();
			delay_ms(250);
			// If a key is pressed, break out of the loop and take action
			if(keyPadKeyPressed != 'N' || snoozeBarPressed) {
				break;
			} // End if
			updateTimeFromRTC(currentTime);
			updateDateFromRTC(currentDate);
			displayDispTime(currentTime, true);
			displayDispDate(currentDate);
			delay_ms(500);
			// If a key is pressed, break out of the loop and take action
			if(keyPadKeyPressed != 'N' || snoozeBarPressed) {
				break;
			} // End if
		} // End for loop

	} // End while loop

	// If snooze bar was pressed, snooze the alarm using alarm 2 of the RTC and notify the user
	if(snoozeBarPressed) {
		snoozeBarPressed = false;
		snoozeAlarm(gSnoozeVal);
		if(gSnoozeVal > 0) {
			flashSettingsChange((char*)"SNOOZ", gSnoozeVal, SETTINGS_VAL_FORMAT_MINS);
			flashSettingsChange((char*)"SNOOZ", gSnoozeVal, SETTINGS_VAL_FORMAT_MINS);
		} else {
			flashSettingsChange((char*)"SNOOZ", SETTINGS_VAL_STRING_OFF, SETTINGS_VAL_FORMAT_STRING);
			flashSettingsChange((char*)"SNOOZ", SETTINGS_VAL_STRING_OFF, SETTINGS_VAL_FORMAT_STRING);
		}
	} // End if

	// Reset key value
	keyPadKeyPressed = 'N';

}

void settings(void) {

	uint8_t cursor = 0;

	while(!snoozeBarPressed) {

		displayClear();

		switch(cursor) {
			case 0:									// Alarm ON/OFF
				displayDispSettingsName((char*) "ALRM");
				if(gAlarmOn) {
					displayDispSettingsValue(SETTINGS_VAL_STRING_ON, SETTINGS_VAL_FORMAT_STRING);
				} else {
					displayDispSettingsValue(SETTINGS_VAL_STRING_OFF, SETTINGS_VAL_FORMAT_STRING);
				} // End if
				// Show navigation arrows
				displayDispRightArrow(true);
				displayDispLeftArrow(false);
				break;

			case 1:									// SNOOZE ON/OFF (if ON, display minutes val)
				displayDispSettingsName((char*) "SNOOZ");
				if(gSnoozeVal != 0) {
					displayDispSettingsValue(gSnoozeVal, SETTINGS_VAL_FORMAT_MINS);
				} else {
					displayDispSettingsValue(SETTINGS_VAL_STRING_OFF, SETTINGS_VAL_FORMAT_STRING);
				} // End if
				// Show navigation arrows
				displayDispRightArrow(true);
				displayDispLeftArrow(true);
				break;

			case 2:									// Format 12H/24H
				displayDispSettingsName((char*) "FRMT");
				if(currentTime->am_pm == H_24) {
					displayDispSettingsValue(SETTINGS_VAL_STRING_H24, SETTINGS_VAL_FORMAT_STRING);
				} else {
					displayDispSettingsValue(SETTINGS_VAL_STRING_AMPM, SETTINGS_VAL_FORMAT_STRING);
				} // End if
				// Show navigation arrows
				displayDispRightArrow(false);
				displayDispLeftArrow(true);
				break;

			default:
				break;

		} // End switch statement

		// If a key is pressed, break out of the loop and take action
		while(keyPadKeyPressed == 'N' && !snoozeBarPressed);

		switch(keyPadKeyPressed) {
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
			case '0':

				// Toggle item
				if(cursor == 0) {			// Toggle Alarm ON/OFF
					toggleAlarmOnOff();
				} else if(cursor == 1) {	// Set Snooze value
					if(keyPadKeyPressed == '0') {
						gSnoozeVal = 0;
					} else {
						if((gSnoozeVal+SNOOZE_MINUTES_INCREMENT) <= SNOOZE_MINUTES_MAX) {
							gSnoozeVal += SNOOZE_MINUTES_INCREMENT;
						} // End if
					} // End if-else
				} else if(cursor == 2) {	// Toggle AM/PM
					toggleHourFormat();
				} // End if-else

				break;

			case '*':	// Move cursor left
				// Decrement cursor's position
				if(cursor > 0) {
					cursor--;
				} else {
					cursor = 0;
				} // End if-else
				break;

			case '#':	// Move cursor right
				// Increment cursor's position
				if(cursor < 2) {
					cursor++;
				} else {
					cursor = 2;
				} // End if-else
				break;

			default:
				break;

		} // End switch statement

		// Reset key value
		keyPadKeyPressed = 'N';

	} // End while loop

	snoozeBarPressed = false;

}

static void flashSettingsChange(char* name, uint8_t value, uint8_t format) {

	displayClear();
	displayDispSettingsName(name);
	displayDispSettingsValue(value, format);
	delay_ms(FLASH_SETTING_CHANGE_DELAY_MS);
	displayClear();

}

static bool setItem(struct timeStruct *time, uint8_t value, uint8_t cursor) {

	if(time == NULL) {
		return false;
	} // End if

	uint8_t tmp;
	uint8_t regOffset = time->reg_offset;

	// Fetch date from global variable currentDate
	uint8_t day = currentDate->day;
	uint8_t month = currentDate->month;

	switch(cursor) {

		case 0:						// Set tens of hour
			if(value > 2) {
				return false;
			} // End if
			if((value == 2) && (time->hours%10 > 3)) {
				return false;
			} // End if
			if((time->am_pm != H_24) && (value > 1)) {
				return false;
			} // End if
			if((time->am_pm != H_24) && (value == 1) && (time->hours%10 > 2)) {
				return false;
			} // End if
			tmp = i2c_Read(I2C_RTC_ADD, RTC_HOURS_REG + regOffset);
			if(time->am_pm == H_24) {
				tmp &= ~(0x3 << RTC_TENS_BIT);
			} else {
				tmp &= ~(1 << RTC_TENS_BIT);
			} // End if-else
			tmp |= (value << RTC_TENS_BIT);
			i2c_Write(I2C_RTC_ADD, RTC_HOURS_REG + regOffset, tmp);
			break;

		case 1:						// Set units of hour
			if(value > 9) {
				return false;
			} // End if
			if((value > 3) && (time->hours >= 20)) {
				return false;
			} // End if
			if((time->am_pm != H_24) && (value > 2) && (time->hours >= 10)) {
				return false;
			} // End if
			tmp = i2c_Read(I2C_RTC_ADD, RTC_HOURS_REG + regOffset);
			tmp &= ~(0x0F);
			tmp |= (value);
			i2c_Write(I2C_RTC_ADD, RTC_HOURS_REG + regOffset, tmp);
			break;

		case 2:						// Set tens of minute
			if(value > 5) {
				return false;
			} // End if
			tmp = i2c_Read(I2C_RTC_ADD, RTC_MINS_REG + regOffset);
			tmp &= ~(0x7 << RTC_TENS_BIT);
			tmp |= (value << RTC_TENS_BIT);
			i2c_Write(I2C_RTC_ADD, RTC_MINS_REG + regOffset, tmp);
			break;

		case 3:						// Set units of minute
			if(value > 9) {
				return false;
			} // End if
			tmp = i2c_Read(I2C_RTC_ADD, RTC_MINS_REG + regOffset);
			tmp &= ~(0x0F);
			tmp |= (value);
			i2c_Write(I2C_RTC_ADD, RTC_MINS_REG + regOffset, tmp);
			break;

		case 4:						// Toggle AM/PM
			if(time->am_pm == H_24) {
				return false;
			} // End if
			tmp = i2c_Read(I2C_RTC_ADD, RTC_HOURS_REG + regOffset);
			tmp ^= (1 << RTC_AM_PM_BIT);
			i2c_Write(I2C_RTC_ADD, RTC_HOURS_REG + regOffset, tmp);
			break;

		case 5:						// Set Month
			if(value > 9) {
				return false;
			} // End if
			if(value == 0) {

				if(month<DEC) {
					month++;
				} else {
					month = JAN;
				} // End inner if-else
				if((month == FEB) && (day > 29)) {
					return false;
				} // End inner if
				if(((month==NOV)||(month==APR)||(month==JUN)||(month==SEP)) && (day > 30)) {
					return false;
				} // End inner if
				tmp = ((month/10) << RTC_TENS_BIT) | (month-(month/10)*10);
				i2c_Write(I2C_RTC_ADD, RTC_MONTH_REG, tmp);

			} else {

				if((value == FEB) && (day > 29)) {
					return false;
				} // End inner if
				if(((value==APR)||(value==JUN)||(value==SEP)) && (day > 30)) {
					return false;
				} // End inner if
				//tmp = i2c_Read(I2C_RTC_ADD, RTC_MONTH_REG);
				i2c_Write(I2C_RTC_ADD, RTC_MONTH_REG, value);

			} // End if-else

			break;

		case 6:						// Set tens of Day
			if(value > 3) {
				return false;
			} // End if
			if((value == 3) && (day%10 > 1)) {
				return false;
			} // End if
			if((month == FEB) && (value > 2)) {
				return false;
			} // End if
			tmp = i2c_Read(I2C_RTC_ADD, RTC_DATE_REG);
			tmp &= ~(0x3 << RTC_TENS_BIT);
			tmp |= (value << RTC_TENS_BIT);
			i2c_Write(I2C_RTC_ADD, RTC_DATE_REG, tmp);
			break;

		case 7:						// Set units of day
			if(value > 9) {
				return false;
			} // End if
			if((value > 1) && (day > 30)) {
				return false;
			} // End if
			if(((month == NOV)||(month == APR)||(month == JUN)||(month == SEP)) && (value>0) && (day>30)) {
				return false;
			}
			tmp = i2c_Read(I2C_RTC_ADD, RTC_DATE_REG);
			tmp &= ~(0x0F);
			tmp |= (value);
			i2c_Write(I2C_RTC_ADD, RTC_DATE_REG, tmp);
			break;

		default:
			return false;
			break;

	} // End inner switch statement

	return true;

}

static void snoozeAlarm(uint8_t minutes) {

	// if snooze is active, set alarm 2 of the RTC
	if(gSnoozeVal > 0) {

		updateTimeFromRTC(currentTime);
		setAlarm2(currentTime->hours, currentTime->minutes, gSnoozeVal);
		alarm2OnOff(true);

	} // End if

}

static void toggleAlarmOnOff(void) {

	// Clear interrupt flag
	clearAlarm1InterruptFlag();

	// Toggle Alarm
	toggleAlarm1();

	gAlarmOn = isAlarm1On();

	if(!gAlarmOn) {
		// Turn off snooze alarm
		alarm2OnOff(false);
	} // End if

}

static void toggleHourFormat(void) {

	uint8_t tmp;
	uint8_t hours;
	uint8_t am_pm;
	uint8_t regOffset;

	struct timeStruct *timeArray[2];
	timeArray[0] = currentTime;
	timeArray[1] = alarm1Time;

	// Update time global variables
	updateTimeFromRTC(currentTime);
	updateTimeFromRTC(alarm1Time);

	// Toggle both current time and alarm time
	for(int i=0; i<2; i++) {

		regOffset = timeArray[i]->reg_offset;
		hours = timeArray[i]->hours;
		am_pm = timeArray[i]->am_pm;

		// Read Hours Register content
		tmp = i2c_Read(I2C_RTC_ADD, RTC_HOURS_REG + regOffset);

		if(am_pm != H_24) {								// If AM/PM -> H24

			if((am_pm == PM) && (hours != 12)) {			// If PM
				// Update hour field
				hours += 12;
				tmp &= ~(0x3 << RTC_TENS_BIT);
				tmp &= ~(0x0F);
				tmp |= ((hours/10) << RTC_TENS_BIT);
				tmp |= (hours-((hours/10)*10));
			} else if(hours == 12) {						// If 12
				// Clear PM bit (not present in 24h mode)
				tmp &= ~(1 << RTC_AM_PM_BIT);
				if(am_pm == AM) {
					tmp &= ~(0x1F);
				} // End inner if
			} // End if

			// Set 24H mode
			tmp &= ~(1 << RTC_12_24_BIT);

		} else {										// If H24 -> AM/PM

			if(hours > 12) {								// If PM
				// Update hour field
				hours -= 12;
				tmp &= ~(0x3 << RTC_TENS_BIT);
				tmp &= ~(0x0F);
				tmp |= ((hours/10) << RTC_TENS_BIT);
				tmp |= (hours-((hours/10)*10));
				// Set PM bit
				tmp |= (1 << RTC_AM_PM_BIT);
			} else if(hours == 12) {						// If 12 PM
				// Set PM bit
				tmp |= (1 << RTC_AM_PM_BIT);
			} else if(hours == 0) {							// If 12 AM
				tmp |= 0x12;
			} // End if-else

			// Set 12H mode
			tmp |= (1 << RTC_12_24_BIT);

		} // End inner if-else

		// Write Hours register
		i2c_Write(I2C_RTC_ADD, RTC_HOURS_REG + regOffset, tmp);
		// Update time global variables
		updateTimeFromRTC(timeArray[i]);

	} // End for loop

	// Cancel snooze, if pending
	clearAlarm2InterruptFlag();
	alarm2OnOff(false);

}
