/**
 * CE4100
 * Nicolas Matschinegg
 *
 * IMPLEMENTATION FILE
 *
 * Description:
 * Implemented: 10/26/16
 * Edited:
 *
 */

// SWITCH "DDRB" BACK TO "DDRA"

#include "Keypad.h"

	int c1;
	int c2;
	int c3;

	int input;

void keypadSetup(void)
{
	DDRA  |= (0x0F);	// Analog pins 0-3 are outputs,	pins 4-7 are inputs
	PORTA |= (0x70);	// Analog pins 0-3 are set high, pins 4-7 are low
	PORTA &= (0xF0);

	// enable interrupts on the PCMSK3
	PCICR |= (1 << PCIE0);
	// set the mast to allow interrupts from the cols
	PCMSK0 |= (1 << PCINT4) | (1 << PCINT5) | (1 << PCINT6);
}

char readKeypad(void)
{

//	input = 0;

//	while(input == 0)		// PROBABLY NEED TO USE SOMETHING OTHER THAN WHILE LOOP IF THIS IS INSIDE OF INTERRUPT
//	{
		PORTA |= (0x0F);
		PORTA &= (0xFE);	// Row 1 is pulled low

		c1 = PINA & (1<<4);
		c2 = PINA & (1<<5);
		c3 = PINA & (1<<6);

		if(c1 == 0){
		return '1';				// number 1 pressed
//		input = 1;
		}
		else
		{
			if(c2 == 0){
			return '2';			// number 2 pressed
//			input = 1;
			}
			else
			{
				if(c3 == 0){
				return '3';		// number 3 pressed
//				input = 1;
				}
				else
				{
					// do nothing
				}
			}
		}

		PORTA |= (0x0F);
		PORTA &= (0xFD);	// Row 2 is pulled low

		c1 = PINA & (1<<4);
		c2 = PINA & (1<<5);
		c3 = PINA & (1<<6);

		if(c1 == 0){
		return '4';				// number 4 pressed
//		input = 1;
		}
		else
		{
			if(c2 == 0){
			return '5';			// number 5 pressed
//			input = 1;
			}
			else
			{
				if(c3 == 0){
				return '6';		// number 6 pressed
//				input = 1;
				}
				else
				{
					// do nothing
				}
			}
		}

		PORTA |= (0x0F);
		PORTA &= (0xFB);	// Row 3 is pulled low

		c1 = PINA & (1<<4);
		c2 = PINA & (1<<5);
		c3 = PINA & (1<<6);

		if(c1 == 0){
		return '7';				// number 7 pressed
//		input = 1;
		}
		else
		{
			if(c2 == 0){
			return '8';			// number 8 pressed
//			input = 1;
			}
			else
			{
				if(c3 == 0){
				return '9';		// number 9 pressed
//				input = 1;
				}
				else
				{
					// do nothing
				}
			}
		}

		PORTA |= (0x0F);
		PORTA &= (0xF7);	// Row 4 is pulled low

		c1 = PINA & (1<<4);
		c2 = PINA & (1<<5);
		c3 = PINA & (1<<6);

		if(c1 == 0){
		return '*';				// " * " key pressed			// HOW DO WE HANDLE "*" AND "#"
//		input = 1;
		}
		else
		{
			if(c2 == 0){
			return '0';			// number 0 pressed
//			input = 1;
			}
			else
			{
				if(c3 == 0){
				return '#';		// " # " key pressed
//				input = 1;
				}
				else
				{
								// do nothing
				}
			}
		}

		// Ground rows to trigger interrupt when a key is pressed
		PORTA &= (0xF0);
		return 'N';
//	}
}

