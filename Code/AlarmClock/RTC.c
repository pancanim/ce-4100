/**
 * CE4100
 * Michele Pancani
 *
 * IMPLEMENTATION FILE
 *
 * Description:
 * Implemented: 12/18/16
 * Edited: 05/28/17
 *
 */

#include "RTC.h"

extern uint8_t settingsTimeColor;
extern uint8_t settingsTimeColor_snoozePending;
static uint8_t sgDefaultTimeColor;

/**
 * Set mask to make alarm 1 and 2 go off every day when hours and minutes match
 */
static void setAlarmMasks(void);

/**
 * Returns true if the RTC is being initialized for the first time, false otherwise
 */
static bool isFirstInit(void);

static void setAlarmMasks(void) {

	uint8_t tmp;

	// Set mask to make alarm 1 go off every day when hours and minutes match
	tmp = i2c_Read(I2C_RTC_ADD, RTC_ALARM1_SECS_REG);
	tmp &= ~(1 << A1M1);
	i2c_Write(I2C_RTC_ADD, RTC_ALARM1_SECS_REG, tmp);

	tmp = i2c_Read(I2C_RTC_ADD, RTC_ALARM1_MINS_REG);
	tmp &= ~(1 << A1M2);
	i2c_Write(I2C_RTC_ADD, RTC_ALARM1_MINS_REG, tmp);

	tmp = i2c_Read(I2C_RTC_ADD, RTC_ALARM1_HOURS_REG);
	tmp &= ~(1 << A1M3);
	i2c_Write(I2C_RTC_ADD, RTC_ALARM1_HOURS_REG, tmp);

	tmp = i2c_Read(I2C_RTC_ADD, RTC_ALARM1_DAYDATE_REG);
	tmp |= (1 << A1M4);
	i2c_Write(I2C_RTC_ADD, RTC_ALARM1_DAYDATE_REG, tmp);

	// Set mask to make alarm 2 go off every day when hours and minutes match
	tmp = i2c_Read(I2C_RTC_ADD, RTC_ALARM2_MINS_REG);
	tmp &= ~(1 << A2M2);
	i2c_Write(I2C_RTC_ADD, RTC_ALARM2_MINS_REG, tmp);

	tmp = i2c_Read(I2C_RTC_ADD, RTC_ALARM2_HOURS_REG);
	tmp &= ~(1 << A2M3);
	i2c_Write(I2C_RTC_ADD, RTC_ALARM2_HOURS_REG, tmp);

	tmp = i2c_Read(I2C_RTC_ADD, RTC_ALARM2_DAYDATE_REG);
	tmp |= (1 << A2M4);
	i2c_Write(I2C_RTC_ADD, RTC_ALARM2_DAYDATE_REG, tmp);

}

static bool isFirstInit(void) {

	uint8_t tmp;
	tmp = i2c_Read(I2C_RTC_ADD, RTC_CONTROL_REG);
	return ((tmp & (1 << MY_BIT)) >> MY_BIT);

}

bool RTCInit(void) {

	// If device has never been initialized before
	if(isFirstInit()) {

		// Use INT/SQW to generate interrupt when alarm goes off and reset MY_BIT (See definition in RTC.h)
		if(i2c_Write(I2C_RTC_ADD, RTC_CONTROL_REG, 0x0C) != 0) {
			return false;
		} // End if

		// Clear OSF and disable 32KHz Output
		if(i2c_Write(I2C_RTC_ADD, RTC_STATUS_REG, 0x00) != 0) {
			return false;
		} // End if

		// Initialize Time/Data/Alarm registers. Default 12:00 AM - 01/01/17
		if(i2c_Write(I2C_RTC_ADD, RTC_SECS_REG, 0x00) != 0) {
			return false;
		} // End if
		if(i2c_Write(I2C_RTC_ADD, RTC_MINS_REG, 0x00) != 0) {
			return false;
		} // End if
		if(i2c_Write(I2C_RTC_ADD, RTC_HOURS_REG, 0x52) != 0) {
			return false;
		} // End if
		if(i2c_Write(I2C_RTC_ADD, RTC_DAY_REG, 0x07) != 0) {
			return false;
		} // End if
		if(i2c_Write(I2C_RTC_ADD, RTC_DATE_REG, 0x01) != 0) {
			return false;
		} // End if
		if(i2c_Write(I2C_RTC_ADD, RTC_MONTH_REG, 0x01) != 0) {
			return false;
		} // End if
		if(i2c_Write(I2C_RTC_ADD, RTC_YEAR_REG, 0x17) != 0) {
			return false;
		} // End if
		if(i2c_Write(I2C_RTC_ADD, RTC_ALARM1_SECS_REG, 0x00) != 0) {
			return false;
		} // End if
		if(i2c_Write(I2C_RTC_ADD, RTC_ALARM1_MINS_REG, 0x00) != 0) {
			return false;
		} // End if
		if(i2c_Write(I2C_RTC_ADD, RTC_ALARM1_HOURS_REG, 0x52) != 0) {
			return false;
		} // End if

	} // End if

	// Set mask to make alarm 1 and 2 go off daily when hours and minutes match
	setAlarmMasks();

	// Clear interrupt flag
	clearAlarm1InterruptFlag();
	clearAlarm2InterruptFlag();

	// Set ALARM_INT as input
	DDRB &= ~(1<<PB2);
	// Enable internal pull-up resistor
	PORTB |= (1<<PB2);
	// Allow SQW to generate interrupt requests on both edges
	PCICR |= (1<<PCIE1);
	// Allow PCINT to trigger interrupts
	PCMSK1 |= (1<<PCINT10);

	sgDefaultTimeColor = settingsTimeColor;
	if(isAlarm2On()) {
		settingsTimeColor = settingsTimeColor_snoozePending;
	} // End if

	return true;

}

bool updateTimeFromRTC(struct timeStruct *time) {

	if(time == NULL) {
		return false;
	} // End if

	uint8_t tmp;
	uint8_t regOffset = time->reg_offset;

	// Read hours
	tmp = i2c_Read(I2C_RTC_ADD, RTC_HOURS_REG + regOffset);
	if(tmp == -1) {
		return false;
	} // End if
	if(((tmp&(1<<RTC_12_24_BIT))>>RTC_12_24_BIT) == H_24) {
		time->am_pm = H_24;
		time->hours = ((tmp&(3<<RTC_TENS_BIT))>>RTC_TENS_BIT)*10 + (tmp&0x0F);
	} else {
		time->am_pm = ((tmp&(1<<RTC_AM_PM_BIT))>>RTC_AM_PM_BIT) + 1;
		time->hours = ((tmp&(1<<RTC_TENS_BIT))>>RTC_TENS_BIT)*10 + (tmp&0x0F);
	} // End if-else

	// Read minutes
	tmp = i2c_Read(I2C_RTC_ADD, RTC_MINS_REG + regOffset);
	if(tmp == -1) {
		return false;
	} // End if
	time->minutes = ((tmp&0x70)>>RTC_TENS_BIT)*10 + (tmp&0x0F);

	return true;

}

bool updateDateFromRTC(struct dateStruct *date) {

	if(date == NULL) {
		return false;
	} // End if

	uint8_t tmp;

	// Read Date
	tmp = i2c_Read(I2C_RTC_ADD, RTC_DATE_REG);
	if(tmp == -1) {
		return false;
	} // End if
	date->day = ((tmp&0x30)>>RTC_TENS_BIT)*10 + (tmp&0x0F);

	// Read Month
	tmp = i2c_Read(I2C_RTC_ADD, RTC_MONTH_REG);
	if(tmp == -1) {
		return false;
	} // End if
	date->month = ((tmp&(1<<RTC_TENS_BIT))>>RTC_TENS_BIT)*10 + (tmp&0x0F);

	return true;

}

bool isAlarm1On(void) {

	uint8_t tmp;
	tmp = i2c_Read(I2C_RTC_ADD, RTC_CONTROL_REG);
	return (tmp & (1 << A1IE)) >> A1IE;

}

bool isAlarm2On(void) {

	uint8_t tmp;
	tmp = i2c_Read(I2C_RTC_ADD, RTC_CONTROL_REG);
	return (tmp & (1 << A2IE)) >> A2IE;

}

void toggleAlarm1(void) {

	uint8_t tmp;
	tmp = i2c_Read(I2C_RTC_ADD, RTC_CONTROL_REG);
	tmp ^= (1 << A1IE);
	i2c_Write(I2C_RTC_ADD, RTC_CONTROL_REG, tmp);

}

void setAlarm2(uint8_t hours, uint8_t minutes, uint8_t snoozeVal) {

	uint8_t tmpHours = 0;
	uint8_t tmpMinutes = 0;
	uint8_t tmpAmPm = 0;
	uint8_t remainingSnoozeVal = snoozeVal;

	extern struct timeStruct *currentTime;

	while(remainingSnoozeVal >= 60) {
		remainingSnoozeVal -= 60;
		tmpHours++;
	} // End while loop

	if((minutes+remainingSnoozeVal) >= 60) {
		tmpMinutes = remainingSnoozeVal - (60-minutes);
		tmpHours++;
	} else {
		tmpMinutes = minutes+remainingSnoozeVal;
	} // End if-else

	if(currentTime->am_pm == H_24) {

		tmpAmPm = H_24;

		if((tmpHours+hours) >= 24) {
			tmpHours = ((tmpHours+hours)-24);
		} else {
			tmpHours += hours;
		} // End if-else

	} else {

		if((tmpHours+hours >= 12) && tmpHours > 0) {
			if(currentTime->am_pm == AM) {
				tmpAmPm = PM;
			} else {
				tmpAmPm = AM;
			} // End if-else
		} else {
			tmpAmPm = currentTime->am_pm;
		} // End if-else

		if((tmpHours+hours) > 12) {
			tmpHours = ((tmpHours+hours)-12);
		} else {
			tmpHours += hours;
		} // End if-else

	} // End if-else

	uint8_t tmp;
	tmp = i2c_Read(I2C_RTC_ADD, RTC_ALARM2_MINS_REG);
	tmp &= ~(0x7F);
	tmp |= (tmpMinutes-((tmpMinutes/10)*10));
	tmp |= ((tmpMinutes/10) << RTC_TENS_BIT);
	i2c_Write(I2C_RTC_ADD, RTC_ALARM2_MINS_REG, tmp);

	tmp = i2c_Read(I2C_RTC_ADD, RTC_ALARM2_HOURS_REG);
	tmp &= ~(0x7F);
	tmp |= (tmpHours-((tmpHours/10)*10));
	tmp |= ((tmpHours/10) << RTC_TENS_BIT);
	if(tmpAmPm == H_24) {
		tmp &= ~(1 << RTC_12_24_BIT);
	} else {
		tmp |= (1 << RTC_12_24_BIT);
		if(tmpAmPm == PM) {
			tmp |= (1 << RTC_AM_PM_BIT);
		} else {
			tmp &= ~(1 << RTC_AM_PM_BIT);
		} // End if-else
	} // End if-else
	i2c_Write(I2C_RTC_ADD, RTC_ALARM2_HOURS_REG, tmp);

}

void alarm2OnOff(bool alarmOn) {

	uint8_t tmp;

	clearAlarm2InterruptFlag();

	tmp = i2c_Read(I2C_RTC_ADD, RTC_CONTROL_REG);

	if(alarmOn) {
		tmp |= (1 << A2IE);
		settingsTimeColor = settingsTimeColor_snoozePending;
	} else {
		tmp &= ~(1 << A2IE);
		settingsTimeColor = sgDefaultTimeColor;
	} // End if-else

	i2c_Write(I2C_RTC_ADD, RTC_CONTROL_REG, tmp);
}

void clearAlarm1InterruptFlag(void) {

	uint8_t tmp;
	tmp = i2c_Read(I2C_RTC_ADD, RTC_STATUS_REG);
	tmp &= ~(1 << A1F);
	i2c_Write(I2C_RTC_ADD, RTC_STATUS_REG, tmp);

}

void clearAlarm2InterruptFlag(void) {

	uint8_t tmp;
	tmp = i2c_Read(I2C_RTC_ADD, RTC_STATUS_REG);
	tmp &= ~(1 << A2F);
	i2c_Write(I2C_RTC_ADD, RTC_STATUS_REG, tmp);

}
