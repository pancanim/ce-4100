/**
 * CE4100
 * Michele Pancani
 *
 * IMPLEMENTATION FILE
 *
 * Description:
 * Implemented: 05/25/17
 * Edited: 05/28/17
 *
 */

#include "AudioPlayer.h"

extern bool snoozeBarPressed;
extern char keyPadKeyPressed;
extern BOOL gAudioIsPlaying;
extern uint8_t settingsVolumePercentage;

static uint8_t* sgAudioBuffer;			// Circular audio buffer
static uint16_t sgAudioBufferPointer;	// Pointer to the audio sample to be played next

void speakerOn(void) {
	PORTC |= (1<<PC7);
}

void speakerOff(void) {
	PORTC &= ~(1<<PC7);
}

bool DACInit(void) {

	// Set Amplifier Shutdown pin as output
	DDRC |= (1<<PC7);

	// Disable Speaker
	speakerOff();

	return true;

}

void playNextAudioSample_ISR(void) {

	// Declare variables
	uint16_t audioSample_100x;
	uint8_t audioSample;

	// Scale audio samples depending on volume settings
	audioSample_100x = sgAudioBuffer[sgAudioBufferPointer] * settingsVolumePercentage;
	audioSample = (uint8_t) (audioSample_100x / 100);

	// Send sample to DAC and increment buffer pointer
	i2c_Write(I2C_DAC_ADD, audioSample>>4, audioSample<<4);
	if(sgAudioBufferPointer<AUDIO_BUFFER_SIZE-1) {
		sgAudioBufferPointer++;
	} else {
		sgAudioBufferPointer = 0;
	} // End if-else

}

bool playAudioFile(char *fileName) {

	// Validate Parameter
	if(fileName == NULL) {
		return false;
	} // End if

	// Declare variables
	UINT byteRead;
	FATFS fileSys;
	WAV_HEADER wavHeader;
	WAV_HEADER_SUBCHUNK2 wavHeaderSubChunk2;
	char dummyBuff[256];

	sgAudioBufferPointer = 0;
	sgAudioBuffer = (uint8_t*) malloc(AUDIO_BUFFER_SIZE);
	if(sgAudioBuffer == NULL) {
		return false;
	} // End if

	// Disable interrupts for display refresh to save resources and reuse timer
	displayInterruptDisable();

	/**
	 * Mount SD card, if inserted, and create File System
	 */
	// Mount SD Card and create File System
	if(pf_mount(&fileSys) != FR_OK) {
		goto audioFileFailed;
	} // End if

	/**
	 * Load Audio File
	 */
	// Open Audio File
	if(pf_open(fileName) != FR_OK) {
		goto audioFileFailed;
	} // End if

	// Read WAV Header
	if(pf_read(&wavHeader, sizeof(wavHeader), &byteRead) != FR_OK) {
		goto audioFileFailed;
	} // End if

	// Make sure header was fully read
	if(byteRead < sizeof(wavHeader)) {
		goto audioFileFailed;
	} // End if

	// Read WAV subchunk2 of header
	if(pf_read(&wavHeaderSubChunk2, sizeof(wavHeaderSubChunk2), &byteRead) != FR_OK) {
		goto audioFileFailed;
	} // End if

	// Make sure subchunk2 was fully read
	if(byteRead < sizeof(wavHeaderSubChunk2)) {
		goto audioFileFailed;
	} // End if

	// Go to the raw data section
	while(wavHeaderSubChunk2.subChunk2ID != WAV_DATA_SECTION_SUBCHUNK_ID) {

		// Skip information relative to Title, Artist etc..
		if(pf_read(dummyBuff, (wavHeaderSubChunk2.subChunk2Size & 0xFF), &byteRead) != FR_OK) {
			goto audioFileFailed;
		} // End if

		// Read next WAV subchunk2 of header
		if(pf_read(&wavHeaderSubChunk2, sizeof(wavHeaderSubChunk2), &byteRead) != FR_OK) {
			goto audioFileFailed;
		} // End if

		// Make sure subchunk2 was fully read
		if(byteRead < sizeof(wavHeaderSubChunk2)) {
			goto audioFileFailed;
		} // End if

	} // End while loop

	/**
	 * Play Audio
	 */
	// Fill the audio buffer before playing the file
	if(pf_read(sgAudioBuffer, AUDIO_BUFFER_SIZE, &byteRead) != FR_OK) {
		goto audioFileFailed;
	} // End if

	// Make sure the audio buffer has been filled
	if(byteRead < AUDIO_BUFFER_SIZE) {
		goto audioFileFailed;
	} // End if

	// Turn on the speaker and start playing the file (Will be done by ISR)
	speakerOn();
	gAudioIsPlaying = TRUE;
	// Calculate the time [us] between 8-bit audio samples
	displayInterruptEnable((uint16_t) (1000/(wavHeader.byteRate/1000.0)));

	// As the audio is playing, fill the circular buffer making sure
		// to not overwrite samples that have not been played yet
	while(keyPadKeyPressed == 'N' && !snoozeBarPressed) {
		while(sgAudioBufferPointer<AUDIO_BUFFER_SIZE/2 - 10);
		if(pf_read(sgAudioBuffer, AUDIO_BUFFER_SIZE/2, &byteRead) != FR_OK) {
			goto audioFileFailed;
		} else if(byteRead < AUDIO_BUFFER_SIZE/2) {
			break;
		}// End if
		while(sgAudioBufferPointer<AUDIO_BUFFER_SIZE - 10);
		if(pf_read(sgAudioBuffer+AUDIO_BUFFER_SIZE/2, AUDIO_BUFFER_SIZE/2, &byteRead) != FR_OK) {
			goto audioFileFailed;
		} else if(byteRead < AUDIO_BUFFER_SIZE/2) {
			break;
		} // End if
	} // End while loop

	// Stop playing the file (played by ISR) and turn off the speaker
	displayInterruptDisable();
	gAudioIsPlaying = FALSE;
	speakerOff();

	// Enable interrupts for display refresh, disabled at the beginning of this function
	displayInterruptEnable(DISP_CLK_HALF_PERIOD_US);

	free(sgAudioBuffer);
	return true;

audioFileFailed:
	free(sgAudioBuffer);
	return false;

}

void playDefaultAlarm(void) {

	sgAudioBufferPointer = 0;
	sgAudioBuffer = (uint8_t*) malloc(AUDIO_BUFFER_SIZE);
	if(sgAudioBuffer == NULL) {
		return;
	} // End if

	// Fill buffer with 5.56 kHz square wave (90us between samples)
	uint16_t tmpCounter=0;
	while(tmpCounter < (AUDIO_BUFFER_SIZE-1)) {
		sgAudioBuffer[tmpCounter++] = 0x00;
		sgAudioBuffer[tmpCounter++] = 0xFF;
	} // End for loop

	// Disable interrupts for display refresh to save resources and reuse timer
	displayInterruptDisable();

	// Turn on the speaker and start playing the file (Will be done by ISR)
	speakerOn();
	gAudioIsPlaying = TRUE;

	// Play 3 beeps (1ms On, 1ms off)
	for(uint8_t reps=0; reps<3; reps++) {
		displayInterruptEnable(90);
		delay_ms(100);
		displayInterruptDisable();
		delay_ms(100);
		if(keyPadKeyPressed != 'N' || snoozeBarPressed) {
			break;
		} // End if
	} // End for-loop

	// Stop playing the file (played by ISR) and turn off the speaker
	displayInterruptDisable();
	gAudioIsPlaying = FALSE;
	speakerOff();

	// Enable interrupts for display refresh, disabled at the beginning of this function
	displayInterruptEnable(DISP_CLK_HALF_PERIOD_US);

	free(sgAudioBuffer);

}
