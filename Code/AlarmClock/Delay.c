/**
 * CE4100
 * Michele Pancani
 *
 * IMPLEMENTATION FILE
 *
 * Description:
 * Implemented: 10/23/16
 * Edited: 11/11/16
 *
 */

#include "Delay.h"

void delay_ms(uint16_t ms) {

	uint8_t delayms;
	uint16_t numberOfLoops;

	extern bool keyPadKeyPressedDelayReset;

	// Check boundary conditions and split the delay in
	// multiple loops, if necessary
	if(ms <= 12) {
		numberOfLoops = 1;
		delayms = ms;
	} else {
		numberOfLoops = (uint16_t) ms/12;
		delayms = 12;
		// If delay is not a multiple of 12ms, compensate for the remainder
		if(ms%12 != 0) {
			delay_ms(ms%12);
		} // End if
	} // End if

	for(uint16_t i=0; i<numberOfLoops; i++) {

		// If interrupt changes conditions such that the
		// delay is no longer needed, break out of the loop
		if(keyPadKeyPressedDelayReset) {
			keyPadKeyPressedDelayReset = false;
			break;
		} // End if


		// Clear compare flag
			// Clear flag at this point in order to cancel
			// all of the following delays when interrupt
			// changes the condition for which they were needed
		TIFR0 |= (1<<OCF0A);

		// Clear timer
		TCNT0 = 0x00;

		// Set OCR with delay value
		OCR0A = (uint8_t) (delayms * CPU_FREQ_MHZ*1000/1024 - 1);

		// Set Clock/1024 from prescaler (19.531 kHz) and start timer
		TCCR0B |= (1<<CS02|1<<CS00);

		// Delay
		while((TIFR0 & (1<<OCF0A)) == 0);

		// Stop timer
		TCCR0B &= ~(1<<CS02|1<<CS01|1<<CS00);

		// Clear compare flag
			// Don't clear flag at this point in order to cancel
			// all of the following delays when interrupt
			// changes the condition for which they were needed
		//TIFR0 |= (1<<OCF0A);

	} // End for loop

}

void delay_us(uint16_t us) {

	uint8_t delayus;
	uint16_t numberOfLoops;

	// Check boundary conditions and split the delay in
	// multiple loops, if necessary
	if(us <= 12) {
		numberOfLoops = 1;
		delayus = us;
	} else {
		numberOfLoops = (uint16_t) us/12;
		delayus = 12;
		// If delay is not a multiple of 12us, compensate for the remainder
		if(us%12 != 0) {
			delay_us(us%12);
		} // End if
	} // End if

	for(uint16_t i=0; i<numberOfLoops; i++) {

		// Clear timer
		TCNT0 = 0x00;

		// Set OCR with delay value
		OCR0A = (uint8_t) (delayus * CPU_FREQ_MHZ - 1);

		// Set Clock without prescaler (20 MHz) and start timer
		TCCR0B |= (1<<CS00);

		// Delay
		while((TIFR0 & (1<<OCF0A)) == 0);

		// Stop timer
		TCCR0B &= ~(1<<CS02|1<<CS01|1<<CS00);

		// Clear compare flag
		TIFR0 |= (1<<OCF0A);

	} // End for loop

}

void start1msSysClock(void) {

	// Clear timer
	TCNT2 = 0x00;

	// Set CTC mode
	TCCR2A |= (1<<WGM21);

	// Set OCR with delay value
	OCR2A = (uint8_t) (1 * CPU_FREQ_MHZ*1000/128 - 1);

	// Enable timer interrupt
	TIMSK2 |= (1<<OCIE2A);

	// Set Clock/128 from prescaler (156.25 kHz) and start timer
	TCCR2B |= (1<<CS22|1<<CS20);

}

void stop1msSysClock(void) {

	// Stop timer
	TCCR2B &= ~(1<<CS22|1<<CS21|1<<CS20);

	// Disable Timer interrupt
	TIMSK2 &= ~(1<<OCIE2A);

}
