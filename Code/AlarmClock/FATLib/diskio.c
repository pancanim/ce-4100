/*-----------------------------------------------------------------------*/
/* Low level disk I/O module skeleton for Petit FatFs (C)ChaN, 2014      */
/*-----------------------------------------------------------------------*/

#include "diskio.h"
#include "../SDCard/sd_io.h"	// 2017.05.14 MP

// 2017.05.14 MP
SD_DEV dev[1];          // Create device descriptor

/*-----------------------------------------------------------------------*/
/* Initialize Disk Drive                                                 */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize (void)
{
	DSTATUS stat;

	// Put your code here
	if(SD_Init(dev)==SD_OK) {
		stat = 0;
	} else {
		stat = STA_NOINIT;
	}// End if

	return stat;
}



/*-----------------------------------------------------------------------*/
/* Read Partial Sector                                                   */
/*-----------------------------------------------------------------------*/

DRESULT disk_readp (
	BYTE* buff,		/* Pointer to the destination object */
	DWORD sector,	/* Sector number (LBA) */
	UINT offset,	/* Offset in the sector */
	UINT count		/* Byte count (bit15:destination) */
)
{

	DRESULT res;

	// Put your code here
	SDRESULTS sdRes;
	sdRes = SD_Read(dev, buff, sector, offset, count);

	// SDRESULTS:
	// SD_NOINIT      /* 1: SD not initialized    */
	// SD_PARERR      /* 3: Invalid parameter     */
	// SD_BUSY        /* 4: Programming busy      */
	// SD_REJECT      /* 5: Reject data           */
	// SD_NORESPONSE  /* 6: No response           */

	// DRESULT:
	// RES_OK		/* 0: Function succeeded */
	// RES_ERROR	/* 1: Disk error */
	// RES_NOTRDY	/* 2: Not ready */
	// RES_PARERR	/* 3: Invalid parameter */
	switch(sdRes) {
	case SD_OK:
		res = RES_OK;
		break;
	case SD_ERROR:
		res = RES_ERROR;
		break;
	case SD_PARERR:
		res = RES_PARERR;
		break;
	case SD_NOINIT:
	case SD_BUSY:
	case SD_REJECT:
	case SD_NORESPONSE:
	default:
		res = RES_NOTRDY;
		break;
	} // End switch statement

	return res;
}



/*-----------------------------------------------------------------------*/
/* Write Partial Sector                                                  */
/*-----------------------------------------------------------------------*/

DRESULT disk_writep (
	BYTE* buff,		/* Pointer to the data to be written, NULL:Initiate/Finalize write operation */
	DWORD sc		/* Sector number (LBA) or Number of bytes to send */
)
{
	DRESULT res;

	res = RES_NOTRDY;	// 2017.05.14 MP
	if (!buff) {
		if (sc) {

			// Initiate write process

		} else {

			// Finalize write process

		}
	} else {

		// Send data to the disk

	}

	return res;
}

