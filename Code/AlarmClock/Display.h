/**
 * CE4100
 * Michele Pancani
 *
 * HEADER FILE
 *
 * Description:
 * Implemented: 10/25/16
 * Edited:
 *
 */

#ifndef DISPLAY_H_
#define DISPLAY_H_

#include <avr/io.h>
#include <avr/interrupt.h>

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#include "Delay.h"
#include "RTC.h"
#include "I2C.h"

#define DISP_CLK_HALF_PERIOD_US 26	// DO NOT CHANGE THIS NUMBER (It was calculated mathematically and adjusted experimentally)
#define DISP_LAT_DELAY_US 40		// (40<= LAT_DELAY <= 1000) (43 fps -> 32 fps)

#define ROWS 16
#define COLS 32
#define MAX_CHARS_PER_ROW 5
#define COLS_PER_CHAR 5
#define ROWS_PER_CHAR 7

#define DISP_R1 2
#define DISP_G1 3
#define DISP_B1 4
#define DISP_R2 5
#define DISP_G2 6
#define DISP_B2 7
#define DISP_A 2
#define DISP_B 3
#define DISP_C 4
#define DISP_LAT 5
#define DISP_CLK 0
#define DISP_OE 1

#define BIT_PER_COLOR 1
#define R 2
#define G 1
#define B 0

#define WHITE 0xFF
#define BLACK 0x00
#define RED 0b100
#define GREEN 0b010
#define BLUE 0b001
#define PURPLE 0b101
#define YELLOW 0b110
#define LIGHT_BLUE 0b011

#define H_24 0
#define AM 1
#define PM 2

#define JAN 1
#define FEB 2
#define MAR 3
#define APR 4
#define MAY 5
#define JUN 6
#define JUL 7
#define AUG 8
#define SEP 9
#define OCT 10
#define NOV 11
#define DEC 12

#define MONDAY		1
#define TUESDAY		2
#define WEDNESDAY	3
#define THURSDAY	4
#define FRIDAY		5
#define SATURDAY	6
#define SUNDAY		7

#define SETTINGS_VOL "VOL"
#define SETTINGS_TEMP "TEMP"

#define SETTINGS_VAL_FORMAT_HEX 0
#define SETTINGS_VAL_FORMAT_PCTG 1
#define SETTINGS_VAL_FORMAT_DEG 2
#define SETTINGS_VAL_FORMAT_STRING 3
#define SETTINGS_VAL_FORMAT_MINS 4

#define SETTINGS_VAL_STRING_ON 0
#define SETTINGS_VAL_STRING_OFF 1
#define SETTINGS_VAL_STRING_AMPM 2
#define SETTINGS_VAL_STRING_H24 3

#define SET_TIME_CURSOR_OFFSET 0
#define SET_TIME_CURSOR_SPAN 7
#define SET_ALARM_CURSOR_OFFSET 0
#define SET_ALARM_CURSOR_SPAN 4
#define SETTINGS_CURSOR_OFFSET 0
#define SETTINGS_CURSOR_SPAN 1

#define SCREEN_SET_TIME 0
#define SCREEN_SET_ALARM 1

/********************************************************************************
 * Function name: displayClearChar
 * Operation: Clears a character at a given position on the LED display
 * Inputs: X and Y coordinates of the top left pixel
 * Returns: bool - False if character outside of the display boundaries.
 * 					True if characters was cleared successfully
 * Alters: None
 * Implemented: 10/29/16
 * Edited:
 *********************************************************************************/
bool displayClearChar(uint8_t x, uint8_t y);

/********************************************************************************
 * Function name: displayClear
 * Operation: Clears the entire LED display
 * Inputs: None
 * Returns: None
 * Alters: None
 * Implemented: 12/19/16
 * Edited:
 *********************************************************************************/
void displayClear(void);

/********************************************************************************
 * Function name: displayDispDate
 * Operation: Display the given date on the LED display
 * Inputs: date struct
 * Returns: None
 * Alters: None
 * Implemented: 10/29/16
 * Edited:
 *********************************************************************************/
void displayDispDate(struct dateStruct *date);

/********************************************************************************
 * Function name: displayInit
 * Operation: Sets the direction of the display's IO pins and allocates a 16x32
 * 				video buffer.
 * Inputs: None
 * Returns: None
 * Alters: None
 * Implemented: 10/29/16
 * Edited:
 *********************************************************************************/
void displayInit(void);

/********************************************************************************
 * Function name: displayTerminate
 * Operation: Clears the video buffer and turns off the display
 * Inputs: None
 * Returns: None
 * Alters: None
 * Implemented: 10/29/16
 * Edited:
 *********************************************************************************/
void displayTerminate(void);

/********************************************************************************
 * Function name: displayDispLeftArrow
 * Operation: Display or remove the left arrow in the settings menu
 * Inputs: display	true to display the arrow, false to celar it
 * Returns: None
 * Alters: None
 * Implemented: 12/19/16
 * Edited:
 *********************************************************************************/
void displayDispLeftArrow(bool display);

/********************************************************************************
 * Function name: displayDispRightArrow
 * Operation: Display or remove the right arrow in the settings menu
 * Inputs: display	true to display the arrow, false to celar it
 * Returns: None
 * Alters: None
 * Implemented: 12/19/16
 * Edited:
 *********************************************************************************/
void displayDispRightArrow(bool display);

/********************************************************************************
 * Function name: displayDispSettingsName
 * Operation: Display the name of the setting on the screen
 * Inputs: str	String to be displayed (Max 5 chars)
 * Returns: None
 * Alters: None
 * Implemented:
 * Edited:
 *********************************************************************************/
void displayDispSettingsName(char *str);

/********************************************************************************
 * Function name: displayDispSettingsValue
 * Operation: Display the value of the setting on the screen
 * Inputs: val		value to be displayed
 * 		   format	format and units of the value. It could be one of the following:
 * 		   			SETTINGS_VAL_FORMAT_HEX		- Hexadecimal format (TODO)
 * 		   			SETTINGS_VAL_FORMAT_PCTG	- Percentage (%)
 * 		   			SETTINGS_VAL_FORMAT_DEG		- Celsius Degrees (�)
 * 		   			SETTINGS_VAL_FORMAT_STRING	- String format
 * 		   			SETTINGS_VAL_FORMAT_MINS	- Minutes (M)
 * Returns: None
 * Alters: None
 * Implemented:
 * Edited:
 *********************************************************************************/
void displayDispSettingsValue(uint8_t val, uint8_t format);

/********************************************************************************
 * Function name: displayDispTime
 * Operation: Display the given time on the LED display
 * Inputs: time struct and boolean Column on:
 * 			am_pm:
 * 				0 -> 24hrs format
 *				1 -> AM
 *				2 -> PM
 *			columnOn:
 *				true -> Column between hours and minutes is on
 *				false -> Column between hours and minutes is off
 * Returns: None
 * Alters: None
 * Implemented: 10/29/16
 * Edited:
 *********************************************************************************/
void displayDispTime(struct timeStruct *time, bool columnOn);

/********************************************************************************
 * Function name: displayDrawCursor
 * Operation: Display a cursor underneath the element being changed in setup
 * Inputs: Cursor number:
 * 			0: hours' tens
 * 			1: hours' units
 * 			2: minutes' tens
 * 			3: minutes' units
 * 			4: AM/PM/24h
 * 			5: Month
 * 			6: Day
 * 			7: Year
 * 			8: Weekday
 *
 * Returns: bool - False if cursor is out of bound.
 * 					True if cursor was displayed successfully
 * Alters: None
 * Implemented: 01/06/17
 * Edited:
 *********************************************************************************/
bool displayDrawCursor(uint8_t screen, uint8_t cursor);

/********************************************************************************
 * Function name: displayDrawChar
 * Operation: Display a character at a given position on the LED display
 * Inputs: ASCII code of the character, X and Y coordinates of the top left pixel
 * 			and RGB color
 * Returns: bool - False if character is not supported or outside of the display
 * 					Boundaries. True if characters was displayed successfully
 * Alters: None
 * Implemented: 10/29/16
 * Edited:
 *********************************************************************************/
bool displayDrawChar(uint8_t ascii, uint8_t x, uint8_t y, uint8_t color);

/********************************************************************************
 * Function name: displayDrawPixel
 * Operation: Display a pixel at a given position on the LED display
 * Inputs: X and Y coordinates of the pixel and RGB color
 * Returns: None
 * Alters: None
 * Implemented: 10/29/16
 * Edited:
 *********************************************************************************/
void displayDrawPixel(uint8_t x, uint8_t y, uint8_t color);

/********************************************************************************
 * Function name: displayDispRefresh
 * Operation: Draws the pixel pattern in the video buffer on the LED display
 * Inputs: None
 * Returns: None
 * Alters: None
 * Implemented: 10/29/16
 * Edited:
 *********************************************************************************/
void displayDispRefresh(void);

/********************************************************************************
 * Function name: displayDispItalianFlag
 * Operation: Display an Italian flag at a given position on the LED display
 * Inputs: X and Y coordinates of the top left pixel
 * Returns: bool - False if flag is outside of the display boundaries.
 	 	 	 	 	 True if characters was displayed successfully
 * Alters: None
 * Implemented: 10/29/16
 * Edited:
 *********************************************************************************/
bool displayDispItalianFlag(uint8_t x, uint8_t y);

/********************************************************************************
 * Function name: displayPrintString
 * Operation: Display a string starting from a given position on the LED display
 * Inputs: Pointer to the array of ASCII codes of the characters, X and Y
 * 			coordinates of the top left pixel of the first character and RGB color
 * 			Note: Max 5 charcters per row
 * Returns: bool - False if string is too long or outside of the display
 * 					Boundaries. True if string was displayed successfully
 * Alters: None
 * Implemented: 10/29/16
 * Edited:
 *********************************************************************************/
bool displayPrintString(char *ptr, uint8_t x, uint8_t y, uint8_t color);

/********************************************************************************
 * Function name: displayInterruptEnable
 * Operation: Start Timer 1 and make it trigger interrupts in order to execute the
 * 				ISR at the given interval to refresh the display
 * Inputs: us	Display refresh rate in units of microseconds
 * Returns: None
 * Alters: None
 * Implemented:
 * Edited:
 *********************************************************************************/
void displayInterruptEnable(uint16_t us);

/********************************************************************************
 * Function name: displayInterruptDisable
 * Operation: Stop Timer 1 to stop refreshing the display
 * Inputs: None
 * Returns: None
 * Alters: None
 * Implemented:
 * Edited:
 *********************************************************************************/
void displayInterruptDisable(void);

/********************************************************************************
 * Function name: displayInterruptSettime
 * Operation: Change the rate at which the display ISR is executed
 * Inputs: us	Display refresh rate in units of microseconds
 * Returns: None
 * Alters: None
 * Implemented:
 * Edited:
 *********************************************************************************/
void displayInterruptSetTime(uint16_t us);

#endif /* DISPLAY_H_ */
